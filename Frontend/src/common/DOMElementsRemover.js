export class DOMElementsRemover {
    #toRemove = [];

    remove(element) {
        this.#toRemove.push(element);
    }

    execute() {
        for (let element of this.#toRemove) {
            element.remove();
        }
    }
}
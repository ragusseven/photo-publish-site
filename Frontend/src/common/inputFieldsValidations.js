import {inputFieldValidation} from "../kolbasa/components/Inputs/InputField";

export class fileNotEmptyValidation extends inputFieldValidation {
    constructor() {
        super(images => images.length !== 0, "Поле не может быть пустым");
    }
}

export class notEmptyValidation extends inputFieldValidation {
    constructor() {
        super((string) => string && string !== '', "Поле не может быть пустым");
    }
}
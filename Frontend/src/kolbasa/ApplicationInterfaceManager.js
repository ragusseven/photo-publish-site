import {MainPage} from "./pages/MainPage";
import {CreatePublicationPage} from "./pages/CreatePublicationPage";
import {PublicationPage} from "./pages/PublicationPage";
import {deleteCookie, FetchAndHandleWithRetry, getCookie, queryParamsToObject, setCookie} from "./common";
import {LoginPage} from "./pages/LoginPage";
import {apiUrl} from "./pages/config";
import {AuthorProfilePage} from "./pages/AuthorProfilePage";
import {RegistrationPage} from "./pages/RegistrationPage";
import {EditPublicationPage} from "./pages/EditPublicationPage";

export class ApplicationInterfaceManager {
    static __PAGES__ = {
        MainPage: () => new MainPage(),
        CreatePublicationPage: () => new CreatePublicationPage(),
        Post: (args) => new PublicationPage(args),
        LoginPage: () => new LoginPage(),
        AuthorPage: (args) => new AuthorProfilePage(args),
        RegistrationPage: () => new RegistrationPage(),
        EditPage: (args) => new EditPublicationPage(args),
    };
    static currentPage;
    static isAuthorized = false;
    static currentUserLogin = undefined;
    static defaultConditionDelegates = [ApplicationInterfaceManager.addMainPageButton, ApplicationInterfaceManager.restoreSearch];
    static authorizedConditionDelegates = [ApplicationInterfaceManager.addPublicationPageButton, ApplicationInterfaceManager.addProfilePageButton];
    static unauthorizedConditionDelegates = [ApplicationInterfaceManager.addLoginButton];

    static async loadBasePage(currentPageNameWithParams) {
        let splitted = currentPageNameWithParams.split('?');
        let pageName = splitted[0];
        await this.loadPage(() => this.__PAGES__[currentPageNameWithParams
            ? currentPageNameWithParams.split('?')[0]
            : 'MainPage'](currentPageNameWithParams
            ? queryParamsToObject(currentPageNameWithParams.split('?')[1])
            : undefined));
    }

    static async initApplication() {
        ApplicationInterfaceManager.isAuthorized = await ApplicationInterfaceManager.checkAuth();
        ApplicationInterfaceManager.addSearch();
        window.addEventListener('popstate', async () =>
            await ApplicationInterfaceManager.loadPageFromUrl());
    }

    static async addRegistrationButton() {
        ApplicationInterfaceManager.addButton('Регистрация', async () => {
            await ApplicationInterfaceManager.loadPage(ApplicationInterfaceManager.__PAGES__.RegistrationPage)
        })
    }

    static async checkAuth() {
        let refreshToken = getCookie('refreshToken');
        if (!refreshToken)
            return false;
        let response = await FetchAndHandleWithRetry(apiUrl + "/refresh-token?" + new URLSearchParams({
            refreshToken: encodeURI(refreshToken)
        }), {
            method: "post"
        }, true, 1);
        if (response.ok) {
            let dataReceived = JSON.parse(await response.text())
            setCookie("accessToken", dataReceived["accessToken"])
            setCookie("refreshToken", dataReceived["refreshToken"])
            setCookie("login", dataReceived["login"])
        }

        return response.ok;
    }

    static async loadPage(pageConstructor) {
        if (this.currentPage) {
            this.currentPage.unloadFrame();
        }
        await this.restoreDefaultCondition();
        this.currentPage = pageConstructor();
        window.history.pushState('data', 'title', this.currentPage.name);
        this.currentPage.loadFrame();
    }


    static async restoreDefaultCondition() {
        let headerElements = document.querySelector('.header').childNodes;
        let headerElementCopy = [];
        for (let element of headerElements) {
            headerElementCopy.push(element);
        }
        for (let element of headerElementCopy) {
            if (element.id !== 'search')
                element.remove();
        }
        for (let delegate of this.defaultConditionDelegates.concat(ApplicationInterfaceManager.isAuthorized ? this.authorizedConditionDelegates :
            this.unauthorizedConditionDelegates)) {
            delegate();
        }
    }

    static addMainPageButton() {
        //ToDo вынести вёрстку
        let button = document.createElement('div');
        button.classList.add('header-button')
        button.setAttribute('tabindex', '0');
        button.setAttribute('id', 'mainPageButton')
        button.classList.add('circle');
        button.addEventListener('click', async () => {
            await ApplicationInterfaceManager.loadPage(() => new MainPage())
        })
        button.addEventListener('keyup', (event) => ApplicationInterfaceManager.buttonEnterEvent(event, async () => {
            await ApplicationInterfaceManager.loadPage(() => new MainPage())
        }));

        let image = document.createElement('img');
        image.src = "/svg/logo.svg";
        image.classList.add('header-button-image');
        image.setAttribute('alt', 'Главная');
        button.appendChild(image);
        document.querySelector('.header').prepend(button);
    }

    static removeMainPageButton() {
        this.#removeElementById('mainPageButton');
    }

    static removePublicationButton() {
        this.#removeElementById('publishPageButton');
    }

    static removeLogoutButton() {
        this.#removeElementById('logoutButton');
    }

    static restoreSearch() {
        document.getElementById('search').style.display = '';
    }

    static removeSearch() {
        document.getElementById('search').style.display = 'none';
    }

    static removeLoginButton() {
        this.#removeElementById('loginButton');
    }

    static addPublicationPageButton() {
        //ToDo вынести вёрстку
        let button = document.createElement('div');
        button.setAttribute('tabindex', '0');
        button.setAttribute('class', 'header-button');
        button.classList.add('circle');
        button.setAttribute('id', 'publishPageButton');
        button.addEventListener('click', async () => {
            ApplicationInterfaceManager.isAuthorized = await ApplicationInterfaceManager.checkAuth();
            if (ApplicationInterfaceManager.isAuthorized)
                await ApplicationInterfaceManager.loadPage(() => new CreatePublicationPage());
            else
                await ApplicationInterfaceManager.loadPage(() => new LoginPage());
        });
        button.addEventListener('keyup', (event) => ApplicationInterfaceManager.buttonEnterEvent(event, async () => {
            ApplicationInterfaceManager.isAuthorized = await ApplicationInterfaceManager.checkAuth();
            if (ApplicationInterfaceManager.isAuthorized)
                await ApplicationInterfaceManager.loadPage(() => new CreatePublicationPage());
            else
                await ApplicationInterfaceManager.loadPage(() => new LoginPage());
        }));
        let image = document.createElement('img');
        image.src = '/svg/add_publication.svg';
        image.classList.add('header-button-image');
        button.appendChild(image);
        document.querySelector('.header').appendChild(button);
    }

    static addLogoutButton() {

        ApplicationInterfaceManager.addImagedButton("Выйти", '/svg/logout.svg', () => {
            logout();
            ApplicationInterfaceManager.isAuthorized = false;
            ApplicationInterfaceManager.loadPage(() => new MainPage());
        }, "logoutButton")

        function logout() {
            const cookies = ["accessToken", "login", "refreshToken"]
            for (const cookie of cookies)
                deleteCookie(cookie);
        }
    }


    static addSearch() {
        //ToDo вынести вёрстку
        let search = document.createElement('global-search');
        search.setAttribute('class', 'search')
        search.setAttribute('id', 'search')
        document.querySelector('.header').insertBefore(search, document.querySelector('.header').firstChild.nextSibling);
    }

    static #generateButton(id, event, label) {
        let button = document.createElement('div');
        button.setAttribute('class', 'header-button');
        button.setAttribute('tabindex', '0');
        button.setAttribute('id', id);
        button.addEventListener('click', event);
        button.addEventListener('keyup', async (e) => await ApplicationInterfaceManager.buttonEnterEvent(e, event))
        button.textContent = label;
        return button;
    }

    static #insertAfter(referenceNode, newNode) {
        referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
    }

    static addButton(label, event, id = label) {
        //ToDo вынести вёрстку
        let button = this.#generateButton(id, event, label);
        let header = document.querySelector('.header');

        header.appendChild(button);
    }

    static buttonEnterEvent = async (event, delegate) => {
        debugger
        if (event.key === 'Enter') {
            await delegate(event);
        }
    }

    static addButtonAfterSearch(label, event, id = label) {
        let button = this.#generateButton(id, event, label);
        let header = document.querySelector('.header');
        this.#insertAfter(header.querySelector('.search'), button);
    }

    static addImagedButton(label, imageUrl, event, id = label) {
        let button = document.createElement('div');
        button.setAttribute('tabindex', '0');
        button.setAttribute('class', 'header-button');
        button.classList.add('circle');
        button.setAttribute('id', id);
        button.addEventListener('click', event);
        button.addEventListener('keyup', async (e) => await ApplicationInterfaceManager.buttonEnterEvent(e, event))
        let image = document.createElement('img');
        image.src = imageUrl;
        image.setAttribute('alt', label);
        image.classList.add('header-button-image');
        button.appendChild(image);
        let header = document.querySelector('.header');
        header.appendChild(button);
    }

    static hideButton(buttonLabel) {
        //ToDo вынести вёрстку
        let button = document.getElementById(buttonLabel);
        button.remove();
    }


    static #removeElementById(id) {
        document.getElementById(id).remove();
    }

    static async loadPageFromUrl() {
        const pathWithQueryString = window.location.href.split('/').at(-1);
        if (pathWithQueryString.length > 1) {
            await ApplicationInterfaceManager.loadBasePage(pathWithQueryString);
        } else {
            await ApplicationInterfaceManager.loadBasePage('MainPage');
        }
    }

    static addLoginButton() {
        ApplicationInterfaceManager.addImagedButton("Логин", '/svg/login.svg', async () => {
            await ApplicationInterfaceManager.loadPage(() => new LoginPage())
        });
    }

    static addProfilePageButton() {
        let login = getCookie('login');
        if (login)
            ApplicationInterfaceManager.addImagedButton('Профиль', '/svg/profile.svg', async () => {
                await ApplicationInterfaceManager.loadPage(() => new AuthorProfilePage({authorId: login}))
            })
    }
}
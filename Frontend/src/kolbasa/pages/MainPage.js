import {Page} from "./Page";
import {ApplicationInterfaceManager} from "../ApplicationInterfaceManager";
import {apiUrl} from "./config";
import {getDocumentHeight} from "../common";
import {FetchAndHandleWithRetry, getCookie} from "../common";


export class MainPage extends Page {
    constructor() {
        super();
        this.name = 'MainPage';
        this.onLoadFunctions = [this.initContentPlate, this.loadItems, this.subscribeScrollEvent];
        this.onUnloadFunctions = [this.clearContent, this.unsubscribeScrollEvent];
    }

    _inCallback = false;
    currentPack = 0;


    loadItems = async () => {
        if (this._inCallback || this.currentPack <= -1) {
            return;
        }
        this._inCallback = true;
        this.currentPack++;
        let response = await FetchAndHandleWithRetry(`${apiUrl}/publications-preview-list/${this.currentPack}`);
        if (response.ok) {
            let posts = await response.json();
            for (let postJson of posts) {
                let postElement = document.createElement('post-plate');
                postElement.constructFromModel(postJson);
                if (postJson.header !== '\n') {
                    document.getElementById('mainFrame').appendChild(postElement);
                } else {
                    this.currentPack = -1;
                }
                this._inCallback = false;
            }

        }
    }
    subscribeScrollEvent = () => {
        window.addEventListener('scroll', this.scrollEvent);
    }

    unsubscribeScrollEvent = () => {
        window.removeEventListener('scroll', this.scrollEvent)
    }

    scrollEvent = async () => {
        if (window.scrollY * 1.1 >= getDocumentHeight() - window.innerHeight) {
            await this.loadItems();
        }
    }

    clearContent = () => {
        document.querySelector(".content").replaceChildren();
        this._inCallback = false;
        this.currentPack = 0;
    }
}

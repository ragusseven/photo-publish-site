import {Page} from "./Page";
import {apiUrl} from "./config";
import mainTemplate from "../../templates/author-profile-card.html";
import {
    fillHtmlTemplateFromJson,
    getCookie,
    objectToQueryParams,
    FetchAndHandleWithRetry,
    flattenObject
} from "../common";
import {DOMElementsRemover} from "../../common/DOMElementsRemover";
import {ApplicationInterfaceManager} from "../ApplicationInterfaceManager";

export class AuthorProfileArguments {
    constructor(authorLogin) {
        this.authorId = authorLogin;
    }
}

export class AuthorProfilePage extends Page {
    constructor(authorProfileArguments) {
        super();
        this.authorId = authorProfileArguments.authorId;
        this.name = "AuthorPage?" + objectToQueryParams({authorId: this.authorId});

        this.onLoadFunctions.push(async () => await this.loadItems(),
            this.loadPagesList,
            this.loadGallery);
        if (this.checkPageOwnedByUser())
            this.onLoadFunctions.push(() => ApplicationInterfaceManager.hideButton("Профиль"), ApplicationInterfaceManager.addLogoutButton);

        this.onUnloadFunctions.push(() => this.clearContent(),
            () => document.querySelector(".content").replaceChildren());

        this.pages = {"Галерея": this.loadGallery, "Фотосессии": this.loadPhotosessionsSlots};
    }

    loadItems = async () => {
        let authorInfo = await FetchAndHandleWithRetry(`${apiUrl}/users/${this.authorId}`);
        authorInfo = await authorInfo.json();
        authorInfo = flattenObject(authorInfo);
        authorInfo['photo'] = authorInfo['photo'] ? apiUrl + '/images/' + authorInfo['photo']
            : '/svg/profile.svg';
        let template = fillHtmlTemplateFromJson(mainTemplate, authorInfo);
        this.#removeUndefinedFields(template);
        document.querySelector('.content').appendChild(template);
    }

    #removeUndefinedFields(template) {
        let fields = template.querySelectorAll(".author-info-field");
        let remover = new DOMElementsRemover();

        for (let field of fields) {
            if (field.lastElementChild.textContent === 'undefined')
                remover.remove(field);
        }
        remover.execute();
    }

    loadSubpage = (event) => {
        document.querySelector('.author-subpage-plate').replaceChildren();
        this.pages[event.target.id]();
    }
    loadPagesList = () => {
        let buttonsList = document.querySelector(".author-pages-buttons");
        for (let buttonLabel in this.pages) {
            let item = document.createElement('li');
            item.setAttribute('tabindex', '0')
            item.textContent = buttonLabel;
            item.addEventListener('click', this.loadSubpage);
            item.addEventListener('keyup', event => ApplicationInterfaceManager.buttonEnterEvent(event, this.loadSubpage));
            item.setAttribute('id', buttonLabel);
            item.setAttribute('class', 'author-pages-button');
            buttonsList.appendChild(item);
        }
    }
    loadGallery = async () => {
        let place = document.querySelector('.author-subpage-plate');
        let response = await FetchAndHandleWithRetry(`${apiUrl}/users/${this.authorId}/publications`);
        if (response.ok) {
            let posts = await response.json();
            posts.forEach(postJson => place
                .appendChild(document.createElement('post-plate')
                    .constructFromModel(postJson)));
        }
    }
    addPhotosessionAdditionComponent = () => {
        let component = document.createElement('photosession-addition-slot');
        document.querySelector('.author-slots-subpage-plate').appendChild(component);

    }
    loadPhotosessionsSlots = async () => {
        let place = document.querySelector('.author-subpage-plate');
        let plate = document.createElement('div');
        if (this.checkPageOwnedByUser()) {
            let buttonContainer = this.getButtonWithContainer();
            plate.appendChild(buttonContainer);
        }

        plate.setAttribute('class', 'author-slots-subpage-plate');
        let response = await FetchAndHandleWithRetry(`${apiUrl}/users/${this.authorId}/photosessions/open`)
        if (response.ok) {
            let slots = await response.json();
            slots.forEach((slotJson) => plate.appendChild(this.#generateSlotElement(slotJson)))
        }
        if (plate.childNodes.length !== 0)
            place.appendChild(plate);
    }

    #generateSlotElement(slotJson) {
        let slotElement = document.createElement('photosession-slot');
        if (this.checkPageOwnedByUser()) {
            slotElement.owned = true;
        }
        slotElement.constructFromModel(slotJson);
        return slotElement;
    }

    getButtonWithContainer() {
        let buttonContainer = document.createElement('div');
        buttonContainer.classList.add('button-container');
        let photosessionAdditionButton = document.createElement('div');
        photosessionAdditionButton.textContent = "Добавить слот";
        photosessionAdditionButton.setAttribute('class', 'accent-button');
        photosessionAdditionButton.addEventListener('click', this.addPhotosessionAdditionComponent);
        buttonContainer.appendChild(photosessionAdditionButton);
        return buttonContainer;
    }

    checkPageOwnedByUser() {
        return getCookie('login') === this.authorId;
    }
}
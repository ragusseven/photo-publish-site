import mainTemplate from "../../templates/publication-page-template.html";
import {Page} from "./Page";
import {ApplicationInterfaceManager} from "../ApplicationInterfaceManager";
import {apiUrl} from "./config";
import {FetchAndHandleWithRetry, getCookie, objectToQueryParams} from "../common";
import {MainPage} from "./MainPage";
import {EditPublicationPage} from "./EditPublicationPage";

export class PostPageArgsDto {
    constructor(postId) {
        this.postId = postId;
    }
}

export class PublicationPage extends Page {
    constructor(postPageArgsDto) {
        super();
        this.name = 'Post?' + objectToQueryParams({postId: postPageArgsDto.postId});
        this.postId = postPageArgsDto.postId;
        this.onLoadFunctions.push(this.loadPost, this.initContentPlate, this.subscribeWindowResizeEvent);
        this.onUnloadFunctions = [this.clearContent, this.unsubscribeWindowResizeEvent];
    }

    authorLogin
    postId
    loadPost = async (node, child) => {
        let response = await FetchAndHandleWithRetry(`${apiUrl}/publications/${this.postId}`);
        if (response.ok) {
            let post = await response.json();
            let post_header = post["header"];
            let post_text = post["text"];
            this.authorLogin = post["authorLogin"]
            this.postId = post["id"];

            let content = mainTemplate
                .replaceAll("{post_header}", post_header)
                .replaceAll("{post_text}", (post_text || ' ').replaceAll('\n', '<br>'))

            let template = document.createElement('template');
            template.innerHTML = content;
            template = template.content;

            if (post_header !== '\n') {
                document.getElementById('content').appendChild(template);
            }

            if (post.images.length > 0) {
                let carousel = document.createElement('image-carousel');
                for (let i of post.images) {
                    carousel.images.push(i);
                }
                document.querySelector('.full-publication-plate').appendChild(carousel);
            }

            if (this.checkPageOwnedByUser()) {
                ApplicationInterfaceManager.addButtonAfterSearch('Удалить', new DeleteButtonEvents(this.postId).removePublicationButtonEvent);
                ApplicationInterfaceManager.addButtonAfterSearch('Редактировать', () => ApplicationInterfaceManager.loadPage(() => new EditPublicationPage({publicationId: this.postId})));
                ApplicationInterfaceManager.removePublicationButton();
                if (window.innerWidth < 1200)
                    ApplicationInterfaceManager.removeSearch();
            }
        }
    }

    checkWindowWidthAndAddSearch() {
        if (window.innerWidth >= 1200)
            ApplicationInterfaceManager.addSearch();
        else
            ApplicationInterfaceManager.removeSearch();
    }

    subscribeWindowResizeEvent = () => {
        window.addEventListener('resize', this.checkWindowWidthAndAddSearch);
    }

    unsubscribeWindowResizeEvent = () => {
        window.removeEventListener('resize', this.checkWindowWidthAndAddSearch);
    }

    checkPageOwnedByUser() {
        console.log(this.authorLogin)
        return getCookie('login') === this.authorLogin;
    }
}

class DeleteButtonEvents {
    constructor(postId) {
        this.postId = postId;
    }

    sendRemovePublicationRequest = async () => {
        return await FetchAndHandleWithRetry(`${apiUrl}/publications/${this.postId}`, {method: 'delete'}, true, 1);
    }

    exactlyRemovePublicationButtonEvent = async (event) => {
        event.target.removeEventListener('click', this.exactlyRemovePublicationButtonEvent);
        let response = await this.sendRemovePublicationRequest();
        if (response.ok) {
            event.target.textContent = "Публикация удалена";
            setTimeout(() => ApplicationInterfaceManager.loadPage(() => new MainPage()), 3000);
        } else {
            event.target.textContent = "Ошибка удаления";
            setTimeout(() => this.buttonfallback(event), 3000)
        }
    }
    buttonfallback = (event) => {
        if (event.target.textContent === "Точно удалить" || event.target.textContent === "Ошибка удаления") {
            event.target.removeEventListener('click', this.exactlyRemovePublicationButtonEvent);
            event.target.textContent = "Удалить";
            setTimeout(() => event.target.addEventListener('click', this.removePublicationButtonEvent), 1000);
        }
    }
    removePublicationButtonEvent = (event) => {
        event.target.removeEventListener('click', this.removePublicationButtonEvent);
        event.target.textContent = "Точно удалить";
        event.target.addEventListener('click', this.exactlyRemovePublicationButtonEvent);
        setTimeout(() => this.buttonfallback(event), 5000);
    }
}
import publishTemplate from "../../templates/create-publication-page.html";
import {Page} from "./Page";
import {ApplicationInterfaceManager} from "../ApplicationInterfaceManager";
import {apiUrl} from "./config";
import {FetchAndHandleWithRetry, fillHtmlTemplateFromJson, getCookie} from "../common";
import {fileNotEmptyValidation, notEmptyValidation} from "../../common/inputFieldsValidations";


export class CreatePublicationPage extends Page {
    constructor() {
        super();
        this.name = 'CreatePublicationPage';
        this.onLoadFunctions = [this.loadPublish];
        this.onUnloadFunctions = [this.clearContent]
        this.onLoadFunctions.push(() => ApplicationInterfaceManager.removePublicationButton());
    }

    previewInput
    imagesInput
    headerInput
    textInput
    loadPublish = () => {
        document.querySelector('.content').appendChild(fillHtmlTemplateFromJson(publishTemplate, {}));
        this.previewInput = document.getElementById('publication_preview');
        this.imagesInput = document.getElementById('publication_photos')
        this.headerInput = document.querySelector('.header-input-field');
        this.textInput = document.querySelector('.publication-text-input-field');

        this.previewInput.validations.push(new fileNotEmptyValidation());
        this.headerInput.validations.push(new notEmptyValidation());

        document.getElementById("submit_publication")
            .addEventListener("click", async (event) => await this.submitButtonEvent(event, this.headerInput.field().value, this.textInput.field().value, this.previewInput.images[0], this.imagesInput.images.map(file => file)));
    }

    submitButtonEvent = async (event, header, text, previewFile, publicationPhotos) => {
        event.target.disabled = true;
        if (document.getElementById('error-message'))
            document.getElementById('error-message').remove();
        let headerValid = await this.headerInput.checkValidity();
        let previewValid = await this.previewInput.checkValidity();
        if (!(headerValid && previewValid)) return
        let formUploadingResult = await this.uploadForm(header, text, previewFile, publicationPhotos);
        if (formUploadingResult.ok) {
            ApplicationInterfaceManager.loadPage(() => ApplicationInterfaceManager.__PAGES__.Post({postId: formUploadingResult.postId}));
        } else {
            document.querySelector('.publish-content-block').prepend(this.getErrorMessage());
            event.target.disabled = false;
        }
    }

    getErrorMessage() {
        let message = document.createElement('div');
        message.textContent = 'Произошла ошибка отправки';
        message.setAttribute('id', 'error-message');
        message.classList.add('error-message');
        return message;
    }

    async uploadForm(header, text, previewFile, publicationPhotos) {
        const formData = new FormData();
        formData.append('header', header);
        formData.append('text', text);
        formData.append('previewFile', previewFile);
        for (let i = 0; i < publicationPhotos.length; i++) {
            formData.append('files', publicationPhotos[i])
        }

        return await this.sendPublicationRequest(formData);
    }

    async sendPublicationRequest(formData) {
        let response = await FetchAndHandleWithRetry(apiUrl + '/publish', {
            method: 'POST',
            body: formData,
        })
        if (response.ok) {
            let value = await response.json()
            console.log('Form submission successful:', response);
            return {ok: response.ok, postId: value};

        }
        console.error('Error submitting form:');
        return {ok: response.ok, postId: undefined};
    }
}
import {Page} from "./Page";
import {ApplicationInterfaceManager} from "../ApplicationInterfaceManager";
import registrationStepOne from "../../templates/registration-login-password-step.html";
import registrationStepTwo from "../../templates/registration-personal-data-step.html";
import registrationStepThree from "../../templates/registration-profile-photo-template.html";
import {apiUrl} from "./config";
import {convertToFormData} from "../common";
import {FetchAndHandleWithRetry} from "../common";
import {LoginPage} from "./LoginPage";
import {inputFieldValidation} from "../components/Inputs/InputField";
import {notEmptyValidation} from "../../common/inputFieldsValidations";

class onlyLatinOrDigitsSymbolsVelidation extends inputFieldValidation {
    constructor(message) {
        super(() => true, message)
        this.validation = this.containsOnlyLatinOrDigits;
    }

    containsOnlyLatinOrDigits(str) {
        return /^[a-zA-Z0-9]+$/.test(str);
    }
}

export class RegistrationPage extends Page {
    constructor() {
        super();
        this.name = 'RegistrationPage';
        this.onLoadFunctions = [this.initContentPlate, this.loadItems];
        this.onUnloadFunctions = [this.clearContent];
        this.registration = {};
    }

    loadItems = async () => {
        let template = document.createElement('template');
        template.innerHTML = registrationStepOne;
        template = template.content;
        template.getElementById('next-button').addEventListener('click',
            async (event) => {
                await this.completeFirstStep(event);
            });

        document.getElementById('content').appendChild(template);

        this.addFieldsValidations();

    }

    addFieldsValidations() {
        let profileName = document.getElementById('Profile_name');
        let login = document.getElementById('Login');
        let password = document.getElementById('Password');
        let repeatPassword = document.getElementById('Repeat_Password');

        profileName.validations.push(new notEmptyValidation());
        login.validations.push(new notEmptyValidation());
        login.validations.push(new onlyLatinOrDigitsSymbolsVelidation("Логин должен содержать только цифры и латинские символы"));
        login.validations.push({
            validation: async (login) => await this.#checkLoginNotTaken(login),
            message: "Этот логин занят",
            byBlurIgnored: true
        });
        password.validations.push(new notEmptyValidation());
        password.validations.push(new onlyLatinOrDigitsSymbolsVelidation("Пароль может содержать только латинские символы или цифры"));

        profileName.setCheckOnBlur();
        login.setCheckOnBlur();
        password.setCheckOnBlur();
        repeatPassword.setCheckOnBlur();

        repeatPassword.validations.push({
            validation: (repeat) => this.validatePassword(password.field().value, repeat),
            message: "Пароли не совпадают"
        })

        repeatPassword.field().addEventListener('blur', async () => await repeatPassword.checkValidity());
    }

    validatePassword = async (password, repeat) => {
        return password === repeat;
    }
    completeFirstStep = async (event) => {
        event.target.disabled = true
        let profileNameField = document.getElementById('Profile_name').field().value;
        let loginField = document.getElementById('Login').field().value;
        let passwordField = document.getElementById('Password').field().value;
        this.registration.profileName = profileNameField;
        this.registration.login = loginField;
        this.registration.password = passwordField;

        if (await this.checkFirstStepFields()) {
            await this.loadSecondStep();
        } else {
            event.target.disabled = false;
        }
    }

    async checkFirstStepFields() {
        let profileName = await document.getElementById('Profile_name').checkValidity();
        let login = await document.getElementById('Login').checkValidity();
        let password = await document.getElementById('Password').checkValidity();
        let repeatPassword = await document.getElementById('Repeat_Password').checkValidity();

        return password && profileName && login && password && repeatPassword;
    }

    #checkLoginNotTaken = async (login) => {
        let response = await FetchAndHandleWithRetry(apiUrl + '/users/' + login, null, false, 1);
        return !response.ok && response.status === 404;
    }


    loadSecondStep = async () => {
        this.clearContent();
        let template = document.createElement('template');
        template.innerHTML = registrationStepTwo;
        template = template.content;
        template.getElementById('next-button').addEventListener('click',
            async () => {
                await this.completeSecondStep();
            });
        document.getElementById('content').appendChild(template);
        document.getElementById('Email').validations.push(new notEmptyValidation());
        document.getElementById('content').appendChild(template);
    }
    completeSecondStep = async () => {
        this.registration.email = document.getElementById('Email').field().value;
        this.registration.name = document.getElementById('Name').field().value;
        this.registration.surName = document.getElementById('Surname').field().value;
        if (document.getElementById('Email').checkValidity()) {
            await this.loadThirdStep();
        }
    }
    loadThirdStep = async () => {
        this.clearContent();
        let template = document.createElement('template');
        template.innerHTML = registrationStepThree;
        template = template.content;
        template.getElementById('complete-registration').addEventListener('click',
            async (event) => {
                event.target.disabled = true;
                console.log(event.target.disabled)
                await this.completeThirdStep()
                if (await this.register()) {
                    await ApplicationInterfaceManager.loadPage(() => new LoginPage());
                } else {
                    this.invalidateForm('Ошибка')
                    event.target.disabled = false;
                    console.log(event.target.disabled)
                }
            }
        );

        document.getElementById('content').appendChild(template);
    }

    invalidateForm(message) {
        let text = document.createElement('div');
        text.textContent = message;
        text.setAttribute('id', 'login-error-message')
        text.classList.add("invalid-field-message");
        document.querySelector('.login-form').appendChild(text);
    }

    completeThirdStep = async () => {
        this.registration.photo = document.getElementById('Photo').images[0];
    }

    register = async () => {
        let response = await FetchAndHandleWithRetry(apiUrl + "/register", {
            method: "post",
            body: convertToFormData(this.registration)
        });
        return response.ok;
    }
}

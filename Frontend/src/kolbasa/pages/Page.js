import {baseContainer} from "../../templates/elements";
import {fillHtmlTemplateFromJson} from "../common";

export class Page {
    constructor() {
        this.name = ''
        this.onLoadFunctions = [];
        this.onUnloadFunctions = [];
        this.loadFrame = async function () {
            for (let fn of this.onLoadFunctions) {
                await fn();
            }
        }

        this.unloadFrame = function () {
            for (let fn of this.onUnloadFunctions) {
                fn();
            }
        }
    }

    initContentPlate() {
        document.querySelector(".content").appendChild(fillHtmlTemplateFromJson(baseContainer, {}));
    }

    clearContent = () => {
        document.querySelector(".content").replaceChildren();
    }
}
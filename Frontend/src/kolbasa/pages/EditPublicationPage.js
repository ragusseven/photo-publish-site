import {CreatePublicationPage} from "./CreatePublicationPage";
import {FetchAndHandleWithRetry, objectToQueryParams, srcToFile} from "../common";
import {apiUrl} from "./config";
import {ApplicationInterfaceManager} from "../ApplicationInterfaceManager";

export class EditPublicationPage extends CreatePublicationPage {
    constructor(props) {
        super();
        this.editingPublicationId = props.publicationId;
        this.name = "EditPage?" + objectToQueryParams({publicationId: this.editingPublicationId});

        this.onLoadFunctions.push(this.loadPostToEdit);
    }

    loadPostToEdit = async () => {
        let editableHeader = document.querySelector('.header-input-field');
        let editableText = document.querySelector('.publication-text-input-field');
        let editablePublicationPreview = document.getElementById('publication_preview');
        let editablePublicationFiles = document.getElementById('publication_photos');

        this.#setFormsDisable(true);

        let response = await FetchAndHandleWithRetry(`${apiUrl}/publications/${this.editingPublicationId}`);
        if (!response.ok) return;
        response = await response.json();

        editableHeader.value = response['header'];
        editableText.value = response['text'];

        editablePublicationPreview.images.push(await srcToFile(apiUrl + '/images/' + response['preview'], response['preview'] + '_preview.png', 'image'));
        editablePublicationPreview.displayImages();

        for (const image of response.images) {
            editablePublicationFiles.images.push(await srcToFile(apiUrl + '/images/' + image, image + '.png', 'image'));
        }
        editablePublicationFiles.displayImages();

        this.#setFormsDisable(false);
    }

    #setFormsDisable(value) {
        let editableHeader = document.querySelector('.header-input-field');
        let editableText = document.querySelector('.publication-text-input-field');
        let editablePublicationPreview = document.getElementById('publication_preview');
        editableHeader.disabled = value;
        editableText.disabled = value;
        editablePublicationPreview.disabled = value;
    }

    async sendPublicationRequest(formData) {
        let response = await FetchAndHandleWithRetry(apiUrl + '/publications/' + this.editingPublicationId, {
            method: 'PUT',
            body: formData,
        })
        if (response.ok) {
            let value = await response.json()
            console.log('Form submission successful:', response);
            return {ok: response.ok, postId: value};

        }
        console.error('Error submitting form:');
        return {ok: response.ok, postId: undefined};
    }
}
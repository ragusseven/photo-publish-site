import loginTemplate from "../../templates/login.html";
import {PublicationPage} from "./PublicationPage";
import {Page} from "./Page";
import {ApplicationInterfaceManager} from "../ApplicationInterfaceManager";
import {apiUrl} from "./config";
import {deleteCookie, FetchAndHandleWithRetry, fillHtmlTemplateFromJson, setCookie} from "../common";
import {MainPage} from "./MainPage";

export class LoginPage extends Page {
    constructor() {
        super();
        this.name = 'LoginPage';

        this.onLoadFunctions = [this.initContentPlate,
            this.loadItems,
            () => ApplicationInterfaceManager
                .hideButton('Логин'),
            ApplicationInterfaceManager.addRegistrationButton];

        this.onUnloadFunctions = [this.clearContent];
    }

    loadItems = async () => {
        let component = fillHtmlTemplateFromJson(loginTemplate, {});
        component
            .getElementById('js-login')
            .addEventListener('click',
                async (event) => await this.loginButtonEvent(event, document.getElementById("Login").value, document.getElementById("Password").value));

        document.getElementById('mainFrame').appendChild(component);
    }

    loginButtonEvent = async (event, login, password) => {
        event.target.disabled = true;

        if (document.getElementById('login-error-message'))
            document.getElementById('login-error-message').remove();

        let resp = await this.login(login, password);
        if (resp && resp === 200) {
            await ApplicationInterfaceManager.loadPage(() => new MainPage());
            return
        }
        this.invalidateForm(resp && resp === 403 ? "Неверный логин или пароль" : "Ошбика входа");

        event.target.disabled = false;
    }

    invalidateForm(message) {
        let text = document.createElement('div');
        text.textContent = message;
        text.setAttribute('id', 'login-error-message')
        text.classList.add("invalid-field-message");
        document.querySelector('.login-form').appendChild(text);
    }

    async login(login, password) {
        debugger
        const dataToSend = JSON.stringify({"login": login, "password": password});
        let dataReceived = "";
        deleteCookie('accessToken');
        deleteCookie('refreshToken');
        deleteCookie('login');
        let resp = await FetchAndHandleWithRetry(apiUrl + "/login", {
            method: "post",
            headers: {"Content-Type": "application/json"},
            body: dataToSend
        }, true, 1);
        if (resp.ok) {
            if (resp.status === 200) {
                dataReceived = JSON.parse(await resp.text())
                setCookie("accessToken", dataReceived["accessToken"])
                setCookie("refreshToken", dataReceived["refreshToken"])
                setCookie("login", dataReceived["login"])
                ApplicationInterfaceManager.isAuthorized = true;
                return resp.status;
            } else {
                console.log("Status: " + resp.status)
                debugger
                return resp.status;
            }
        } else {
            return resp ? resp.status : false;
        }
    }
}
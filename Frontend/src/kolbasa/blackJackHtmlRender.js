import {getCookie, getFullHeight} from './common.js';
import {ApplicationInterfaceManager} from "./ApplicationInterfaceManager";
import {InputField} from "./components/Inputs/InputField.js"
import {PostPlate} from "./components/PostPlate";
import {DragAndDropImageForm} from "./components/DragAndDropImageForm";
import {Header} from "./components/Header";
import {PhotosessionSlot} from "./components/PhotosessionSlot";
import {PhotosessionAdditionSlot} from "./components/PhotosessionAdditionSlot";
import {Search} from "./components/Search";
import {RecordSlot} from "./components/RecordSlot";
import {Carousel} from "./components/Carousel";
import {FileInputField} from "./components/Inputs/FileInputField";
import {TextAreaInputField} from "./components/Inputs/TextAreaInputFiels";

document.addEventListener("DOMContentLoaded", async function (event) {
    customElements.define('post-plate', PostPlate);
    customElements.define('input-field', InputField);
    customElements.define('drag-and-drop-image-form', DragAndDropImageForm);
    customElements.define('drag-and-drop-input-image-form', FileInputField);
    customElements.define('app-header', Header);
    customElements.define('photosession-slot', PhotosessionSlot);
    customElements.define('photosession-addition-slot', PhotosessionAdditionSlot);
    customElements.define('global-search', Search);
    customElements.define('record-slot', RecordSlot);
    customElements.define('image-carousel', Carousel);
    customElements.define('textarea-input-field', TextAreaInputField);
    await ApplicationInterfaceManager.initApplication();
    await ApplicationInterfaceManager.loadPageFromUrl();
    document.querySelector('.dummyShit').style.height = getFullHeight(document.querySelector('.header')) + 'px';
    window.addEventListener('resize', () => {
        document.querySelector('.dummyShit').style.height = getFullHeight(document.querySelector('.header')) + 'px';
    })
});

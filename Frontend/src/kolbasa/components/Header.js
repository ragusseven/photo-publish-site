import {getFullHeight} from "../common";

export class Header extends HTMLElement {
    connectedCallback() {
        let replacer = document.createElement('div');
        replacer.setAttribute('class', 'dummyShit');
        replacer.style.display = 'flex';
        replacer.style.height = getFullHeight(this) + 'px';

        this.style.position = 'fixed';
        this.parentNode.insertBefore(replacer, this.nextSibling);

        window.addEventListener('scroll', this.onScrollEvent);
    }

    lastScroll = 0;

    onScrollEvent = () => {
        const currentScroll = pageYOffset;
        let top = parseInt(this.style.top) || 0;
        let g = currentScroll - this.lastScroll;
        let s = top - g;
        let n = currentScroll > this.lastScroll
            ? Math.max(-getFullHeight(this), Math.min(top, s))
            : Math.min(0, Math.max(top, s));
        this.style.top = n + 'px';
        this.lastScroll = currentScroll;
    }
}
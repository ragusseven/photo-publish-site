import mainTemplate from "../../templates/main.html";
import {apiUrl} from "../pages/config";
import {ApplicationInterfaceManager} from "../ApplicationInterfaceManager";
import {AuthorProfileArguments, AuthorProfilePage} from "../pages/AuthorProfilePage";
import {PublicationPage, PostPageArgsDto} from "../pages/PublicationPage";

export class PostPlate extends HTMLElement {
    constructor() {
        super();
    }

    id;
    header;
    text;
    author;
    imageSrc;
    authorLogin

    constructFromModel(postJson) {
        this.setAttribute('id', postJson.id);
        this.setAttribute('tabindex', '0');
        this.id = postJson.id;
        this.imageSrc = postJson.preview;
        this.header = postJson.header;
        this.text = postJson.text;
        this.author = postJson.authorProfileName;
        this.authorLogin = postJson.authorLogin;

        return this;
    }

    openPostPage = async (clickEvent) => {
        if (clickEvent.target.matches(['.publication-preview-plate', '.publication-preview-content', '.publication-header', '.publication-text', '.preview-image'])) await ApplicationInterfaceManager
            .loadPage(() => new PublicationPage(new PostPageArgsDto(this.id)));
    };
    enterPressedEvent = async (event) => {
        if (event.key === 'Enter')
            await ApplicationInterfaceManager
                .loadPage(() => new PublicationPage(new PostPageArgsDto(this.id)));
    }
    openAuthorProfilePage = async (e) => {
        if (e.target.matches('.author-link')) {
            await ApplicationInterfaceManager
                .loadPage(() => new AuthorProfilePage(new AuthorProfileArguments(this.authorLogin)));
        }
    };
    events = {
        click: [this.openPostPage, this.openAuthorProfilePage],
        resize: [this.resizePost],
        keyup: [this.enterPressedEvent]
    }

    subscribeEvents() {
        for (let target in this.events) {
            for (let event of this.events[target]) {
                this.addEventListener(target, event);
            }
        }
    }

    connectedCallback() {
        this.setAttribute('class', 'publication-preview-plate')
        this.generatePost(this.imageSrc, this.header, this.text, this.author);
        this.subscribeEvents()
    }


    generatePost(image_url, header, text, author) {
        let img_url = this.generateImage(image_url);
        this.innerHTML = mainTemplate.replaceAll("{post_header}", header)
            .replaceAll("{post_text}", text)
            .replaceAll("{post_author_name}", author);
        this.appendChild(img_url);

    }

    generateImage(img_urll) {
        let img_url = new Image();
        img_url.classList.add("preview-image");
        img_url.onload = () => this.resizePost();
        img_url.src = apiUrl + '/images/' + img_urll;
        return img_url;
    }

    resizePost() {
        if (window.innerWidth <= 1200) return;
        let content = this.firstElementChild;
        let image = this.lastElementChild;
        if (!content || !image) return;
        let height = image.offsetHeight;
        let width = image.offsetWidth;
        let newWidth = this.offsetWidth * 0.6;
        let newHeight = 600;
        let newHeightByNewWidth = height * (newWidth / width);
        let newWidthByNewHeight = width * (newHeight / height);
        if (newHeightByNewWidth <= newHeight) {
            image.style.height = newHeightByNewWidth + 'px';
            image.style.width = newWidth + 'px';
        } else if (newWidthByNewHeight <= newWidth) {
            image.style.height = newHeight + 'px';
            image.style.width = newWidthByNewHeight + 'px';
        }
        this.style.height = image.style.height;
        content.style.maxWidth = `calc(100% - ${this.lastElementChild.offsetWidth})`;
        console.log(content.style.maxHeight);
    }
}
import {convertToFormData, FetchAndHandleWithRetry, fillHtmlTemplateFromJson, getCookie} from "../common";
import slotTemplate from "../../templates/record-slot.html";
import {apiUrl} from "../pages/config";
import {PhotosessionSlot} from "./PhotosessionSlot";

export class RecordSlot extends PhotosessionSlot {
    constructor() {
        super();
    }

    email
    name
    id
    photosessionParent

    constructFromModel(slotJson) {
        this.setAttribute('tabindex', '0');
        this.email = slotJson.email;
        this.name = slotJson.name;
        this.id = slotJson.id;
    }

    connectedCallback() {
        let slotElement = fillHtmlTemplateFromJson(slotTemplate.replaceAll('{id}', this.id), {
            email: this.email,
            name: this.name
        });
        this.replaceChildren();
        this.appendChild(slotElement);
        this.classList.add('record-slot');

        let button = document.getElementById(`submit-button-${this.id}`);
        button.addEventListener('click', this.submitRecord)
    }

    submitRecord = async () => {
        let response = await FetchAndHandleWithRetry(apiUrl + `/photosessions/${this.photosessionParent.id}/records/${this.id}`, {
            method: 'PUT', body: convertToFormData({
                status: 2
            })
        });
        if (response.ok) {
            this.transformToSuccess();
        }
    }

    successFinalize = () => this.photosessionParent.remove();
}
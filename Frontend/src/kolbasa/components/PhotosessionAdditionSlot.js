import additionSlotTemplate from "../../templates/photosession-addition-slot.html";
import {apiUrl} from "../pages/config";
import {convertToFormData, FetchAndHandleWithRetry, getCookie} from "../common";
import {ApplicationInterfaceManager} from "../ApplicationInterfaceManager";
import {PhotosessionSlot} from "./PhotosessionSlot";
import {notEmptyValidation} from "../../common/inputFieldsValidations";

export class PhotosessionAdditionSlot extends PhotosessionSlot {
    constructor() {
        super();
    }

    static id = 0;
    id;
    baseConditionInnerComponent;

    connectedCallback() {
        this.constructBase();
    }

    constructBase = () => {
        let template = document.createElement('template');
        template.innerHTML = additionSlotTemplate.replaceAll('{id}', PhotosessionAdditionSlot.id);
        this.id = PhotosessionAdditionSlot.id;
        PhotosessionAdditionSlot.id += 1;

        let component = template.content;
        this.appendChild(component);

        let button = document.getElementById(`submit-button-${this.id}`)
        button.addEventListener('click', this.addButtonEvent)

        let closeButton = this.generateCloseButton();
        closeButton.addEventListener('click', () => this.remove());
        this.appendChild(closeButton);

        document.getElementById(`description-${this.id}`).setCheckOnBlur().validations.push(new notEmptyValidation());
        document.getElementById(`photosession-cost-${this.id}`).setCheckOnBlur().validations.push(new notEmptyValidation(),
            {
                validation: (value) => !!+value && Number.isInteger(+value),
                message: "Цена должна целым быть числом",
                byBlurIgnored: false
            },
            {
                validation: (value) => !!+value && parseInt(value) < 1000000000,
                message: "О, да у нас тут тестировщик!",
                byBlurIgnored: false
            });
        let setValidations = (value) => {
            value.validations.push(new notEmptyValidation())
            value.setCheckOnBlur();
        }
        document.querySelectorAll('.time-input').forEach(setValidations);
        document.querySelectorAll('.input-with-label.date').forEach(setValidations);

        this.baseConditionInnerComponent = this.firstElementChild;
    }

    morphToBase = () => {
        if (!this.baseConditionInnerComponent)
            this.constructBase();
        this.replaceChildren();
        this.appendChild(this.baseConditionInnerComponent);
    }
    successFinalize = () => {
        this.remove();
    }

    errorFinalize = () => {
        this.morphToBase();
    }

    async #asyncEvery(array, predicate) {
        let result = true;
        for (let element of array) {

            result = await predicate(element) && result;
        }
        return result;
    }

    addButtonEvent = async () => {
        let startTime = document.getElementById(`start-time-${this.id}`).field().value;
        let endTime = document.getElementById(`end-time-${this.id}`).field().value;
        let description = document.getElementById(`description-${this.id}`);
        let date = document.getElementById(`photosession-date-${this.id}`);
        let price = document.getElementById(`photosession-cost-${this.id}`);

        let descriptionValid = await description.checkValidity();
        let priceValid = await price.checkValidity();
        let dateValid = await document.querySelector('.input-with-label.date').checkValidity();

        let timeValid = await this.#asyncEvery(document.querySelectorAll('.time-input'),
            async (element) => await element.checkValidity())

        if (!descriptionValid || !priceValid || !dateValid || !timeValid) return;

        if (await this.sendPhotosessionAdditionRequest(this, startTime, endTime, description.field().value, date.field().value, price.field().value)) {
            this.transformToSuccess();
        } else {
            this.transformToError();
        }
    }

    async sendPhotosessionAdditionRequest(slot, startTime, endTime, description, date, price) {
        let response = await FetchAndHandleWithRetry(apiUrl + '/photosessions', {
            method: 'POST',
            body: convertToFormData({
                name: description,
                startDate: date + 'T' + startTime,
                endDate: date + 'T' + endTime,
                price: price,
                status: 1
            }),
        });
        return response && response.ok;
    }
}
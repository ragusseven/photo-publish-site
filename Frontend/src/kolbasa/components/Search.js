import {FetchAndHandleWithRetry, getCookie, sleep} from "../common";
import {apiUrl} from "../pages/config";

export class Search extends HTMLElement {
    constructor(props) {
        super(props);
    }

    search;
    postContainer;
    searchContentContainer;
    inputBar;
    background;

    connectedCallback() {
        this.inputBar = this.#generateInputBar();
        this.search = this.#generateSearchBar();
        this.searchContentContainer = this.#generateSearchContentContainer();
        this.background = this.#generateBackground();
        this.postContainer = this.#generatePostContainer();

        this.search.appendChild(this.inputBar);
        this.searchContentContainer.appendChild(this.search);
        this.searchContentContainer.appendChild(this.postContainer);
        this.appendChild(this.background);
        this.appendChild(this.searchContentContainer);

        window.addEventListener('resize', this.recalculateSearchHeight)
    }

    recalculateSearchHeight = () => {
        this.search.style.height = this.parentElement.offsetHeight + 'px';
    }

    #generatePostContainer() {
        let postContainer = document.createElement('div');
        postContainer.style.display = 'none';
        postContainer.classList.add('post-container');
        return postContainer
    }

    #generateSearchContentContainer() {
        let searchContentContainer = document.createElement('div');
        searchContentContainer.classList.add('search-content');
        return searchContentContainer
    }

    #generateBackground() {
        let background = document.createElement('div');
        background.setAttribute('class', 'modal-background');
        background.style.display = 'none';
        background.addEventListener('click', this.closeSearchBackgroundEvent);
        return background;
    }

    #generateSearchBar() {
        let search = document.createElement('div');
        search.setAttribute('class', 'bar');
        search.style.height = this.parentElement.offsetHeight + 'px';
        return search
    }

    #generateInputBar() {
        let inputBar = document.createElement('input');
        inputBar.setAttribute('class', 'input-bar');
        inputBar.addEventListener('focus', this.openSearch);
        inputBar.setAttribute('placeholder', 'Поиск');
        inputBar.addEventListener('input', this.updateSearchResult);
        inputBar.addEventListener('blur', this.closeSearchBlur);
        return inputBar;
    }

    openSearch = () => {
        document.body.addEventListener('keyup', this.closeSearchKeypress)

        this.inputBar.removeEventListener('focus', this.openSearch);

        this.searchContentContainer.classList.add('active');
        this.search.classList.add('active');

        this.background.style.display = "block";
        this.background.style.zIndex = "-1";

        this.searchContentContainer.style.width = this.parentElement.offsetWidth + 'px';

        this.style.position = 'fixed';
        this.style.width = this.parentElement.offsetWidth + 'px';

        document.body.style.overflow = "hidden";

        this.updateSearchResult();
    }

    updateSearchResult = async () => {
        if (this.inputBar.value === '')
            return

        let response = await FetchAndHandleWithRetry(`${apiUrl}/publications-find/${this.inputBar.value}`)
        if (!response.ok) return

        this.searchContentContainer.style.minHeight = this.searchContentContainer.offsetHeight + 'px';
        this.postContainer.replaceChildren();

        let posts = await response.json();

        if (posts.length === 0) {
            this.searchContentContainer.classList.remove('contains-post');
            this.postContainer.style.display = 'none';
        } else {
            this.postContainer.style.display = 'flex';
            this.searchContentContainer.classList.add('contains-post');
        }

        posts.forEach((postJson) => this.postContainer.appendChild(this.#generatePostElement(postJson)));
        this.searchContentContainer.style.minHeight = '';
    }

    #generatePostElement(postJson) {
        let postElement = document.createElement('post-plate');
        postElement.constructFromModel(postJson);
        postElement.addEventListener('click', this.closeSearch)
        return postElement;
    }

    closeSearch = async () => {
        document.body.removeEventListener('keyup', this.closeSearchKeypress);

        this.style.display = 'none';
        this.style.position = '';
        this.style.width = '';
        this.style.display = '';

        this.searchContentContainer.style.width = '';
        this.searchContentContainer.classList.remove('active');
        this.searchContentContainer.classList.remove('contains-post');


        this.background.style.display = "none";

        this.postContainer.replaceChildren();
        this.postContainer.style.display = 'none';

        this.search.classList.remove('active');

        document.body.style.overflow = "auto";

        this.inputBar.addEventListener('focus', this.openSearch);
    }

    closeSearchBackgroundEvent = (event) => {
        if (event.target.matches(['.modal-background']))
            this.closeSearch();
    }

    closeSearchKeypress = (e) => {
        debugger
        if (e.key === 'Escape')
            this.closeSearch();
    }

    closeSearchBlur = (event) => {
        if (event.target.value === '')
            this.closeSearch();
    }
}

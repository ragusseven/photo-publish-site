import slotTemplate from "../../templates/photosession-slot.html";
import {convertToFormData, FetchAndHandleWithRetry, fillHtmlTemplateFromJson, getCookie} from "../common";
import recordTemplate from "../../templates/photosession-record-slot.html";
import successTemplate from "../../templates/photosession-slot-success.html";
import errorTemplate from "../../templates/photosession-slot-error.html";
import {apiUrl} from "../pages/config";
import {DOMElementsRemover} from "../../common/DOMElementsRemover";
import {notEmptyValidation} from "../../common/inputFieldsValidations";

function getTimeStringFromDate(startDateTime) {
    function addNumbersIfNeed(time) {
        if (time < 10) return '0' + time; else return '' + time;
    }

    return addNumbersIfNeed(startDateTime.getHours()) + ':' + addNumbersIfNeed(startDateTime.getMinutes());
}

function calculateDiffrenceString(date1, date2) {

    let diff = date2.getTime() - date1.getTime();

    let days = Math.floor(diff / (1000 * 60 * 60 * 24));
    diff -= days * (1000 * 60 * 60 * 24);

    let hours = Math.floor(diff / (1000 * 60 * 60));
    diff -= hours * (1000 * 60 * 60);

    let mins = Math.floor(diff / (1000 * 60));
    diff -= mins * (1000 * 60);

    let seconds = Math.floor(diff / (1000));
    diff -= seconds * (1000);

    if (hours === 0) {
        if (mins !== 0)
            return "меньше часа";
        else {
            return "нули чтоб их";
        }
    }
    if (hours % 10 === 1 && (hours % 100) / 10 !== 1)
        return hours + " час";
    if ([2, 3, 4].includes(hours % 10) && (hours % 100) / 10 !== 1)
        return hours + " часа";
    return hours + " часов";
}

export class PhotosessionSlot extends HTMLElement {
    constructor() {
        super();
    }

    startDate
    endDate
    totalTime
    name
    authorProfileName
    price
    id
    owned;

    constructFromModel(slotJson) {
        let startDateTime = new Date(slotJson.startDate);
        let endDateTime = new Date(slotJson.endDate);
        this.startDate = getTimeStringFromDate(startDateTime);
        this.endDate = getTimeStringFromDate(endDateTime);
        this.totalTime = calculateDiffrenceString(startDateTime, endDateTime)
        this.name = slotJson.name;
        this.authorProfileName = slotJson.authorProfileName;
        this.price = slotJson.price;
        this.id = slotJson.id;
    }

    connectedCallback() {
        this.constructBase();
    }

    constructBase = () => {
        let slotElement = fillHtmlTemplateFromJson(slotTemplate, {
            startDate: this.startDate,
            endDate: this.endDate,
            totalTime: this.totalTime,
            name: this.name,
            authorProfileName: this.authorProfileName,
            price: this.price
        });
        this.replaceChildren();
        this.appendChild(slotElement);
        this.removeEventListener('click', this.transformToBase);
        if (!this.owned) this.addEventListener('click', this.transformToRecordClickEvent); else {
            this.addButton();
            this.addEventListener('click', this.getRecords);
        }

        this.classList.add('photosession-slot-container');
    }
    removeButton;

    addButton() {
        let button = this.generateCloseButton();
        button.addEventListener('click', this.removePhotosession)
        this.removeButton = button;
        this.removeButton.style.display = 'none';
        this.appendChild(button);
    }

    generateCloseButton() {
        let button = document.createElement('button');
        button.classList.add('preview-remove');
        let image = document.createElement('img');
        image.src = '/svg/close-button.svg';
        image.setAttribute('alt', 'x');
        image.classList.add('close-button-ico')
        button.appendChild(image);
        return button;
    }

    removePhotosession = async () => {
        let response = await FetchAndHandleWithRetry(apiUrl + `/photosessions/${this.id}`, {method: 'DELETE'});
        if (response.ok)
            this.remove();
    }

    transformToRecordClickEvent(event) {
        if (!event.target.matches('button')) {
            this.transformToRecordSlot();
        }
    }

    transformToRecordSlot() {
        this.transformToTemplate(recordTemplate);
        this.removeEventListener('click', this.transformToRecordClickEvent);
        this.addEventListener('click', this.transformToBase);

        document.getElementById(`submit-button-${this.id}`).addEventListener('click', this.recordButtonEvent);
        document.getElementById(`email-${this.id}`).validations.push(new notEmptyValidation());
        document.getElementById(`name-${this.id}`).validations.push(new notEmptyValidation());
    }

    components = {}

    transformToTemplate(html) {
        if (this.components[html]) {
            this.replaceChildren(this.components[html]);
            return;
        }
        let template = document.createElement('template');
        template.innerHTML = html.replaceAll('{id}', this.id);
        this.replaceChildren();
        this.appendChild(template.content);
        this.components[html] = this.firstElementChild;
    }

    recordButtonEvent = async (event) => {
        let email = document.getElementById(`email-${this.id}`).field().value;
        let name = document.getElementById(`name-${this.id}`).field().value;
        let checkEmail = await document.getElementById(`email-${this.id}`).checkValidity()
        let checkName = await document.getElementById(`name-${this.id}`).checkValidity()
        debugger
        if (checkName && checkEmail) {
            event.target.disabled = true;
            if (await this.recordToPhotosession(email, name)) {
                this.transformToSuccess();
            } else {
                this.transformToError();
            }
            event.target.disabled = false;
        }
    }

    transformToSuccess = () => {
        this.transformToTemplate(successTemplate);
        setTimeout(this.successFinalize, 3000);
    };
    transformToError = () => {
        this.transformToTemplate(errorTemplate);
        setTimeout(this.errorFinalize, 3000);
    };

    successFinalize = this.constructBase;
    errorFinalize = this.transformToRecordSlot;

    recordToPhotosession = async (email, name) => {
        let response = await FetchAndHandleWithRetry(apiUrl + `/photosessions/${this.id}/records`, {
            method: 'POST', body: convertToFormData({
                email: email, name: name
            })
        });
        return response.ok;
    }

    transformToBase(event) {
        if (event.target.matches(['.photosession-record-slot', '.left-block', '.header'])) this.connectedCallback();
    }

    removeRecords = () => {
        if (!this.querySelector('.photosession-slot').contains(event.target))
            return
        this.removeEventListener('click', this.removeRecords);
        this.addEventListener('click', this.getRecords);
        this.removeButton.style.display = 'none';

        Array.from(this.children)
            .filter((element) => element.classList.contains('record-slot'))
            .forEach((element) => element.remove());

    }
    getRecords = async (event) => {
        this.removeEventListener('click', this.getRecords);
        this.addEventListener('click', this.removeRecords);
        this.removeButton.style.display = '';
        let response = await FetchAndHandleWithRetry(`${apiUrl}/photosessions/${this.id}/records`);
        if (!response.ok) return;

        let records = await response.json();
        records.forEach((slotJson) => this.appendChild(this.#generateSlotElement(slotJson)))
    }

    #generateSlotElement(slotJson) {
        let slotElement = document.createElement('record-slot');
        slotElement.constructFromModel(slotJson);
        slotElement.photosessionParent = this;
        return slotElement;
    }
}
import {fillHtmlTemplateFromJson} from "../common";
import carouselTemplate from "../../templates/carousel-template.html"
import {apiUrl} from "../pages/config";

export class Carousel extends HTMLElement {
    constructor() {
        super();
        this.images = [];
    }

    fillImages() {
        let crslList = this.querySelector('.carousel-list');
        let index = 0;
        for (let image of this.images) {
            let templ = document.createElement('template');
            templ.innerHTML = `<li class="carousel-element"><img src="${apiUrl + '/images/' + image}" alt="${index}"></li>`
            crslList.appendChild(templ.content);
            index += 1;
        }
    }

    connectedCallback() {
        if (this.images.length === 0)
            return
        let id = fillHtmlTemplateFromJson(carouselTemplate, {})
        this.appendChild(id);
        this.fillImages();
        this.crslRoot = this.querySelector('.carousel');
        this.crslList = this.querySelector('.carousel-list');
        this.crslElements = this.querySelectorAll('.carousel-element');
        this.crslElemFirst = this.querySelector('.carousel-element');
        this.leftArrow = this.querySelector('.carousel-arrow-left');
        this.rightArrow = this.querySelector('.carousel-arrow-right');
        this.indicatorDots = this.querySelector('div.carousel-dots');

        // Initialization
        this.options = {
            elemVisible: 1, // Кол-во отображаемых элементов в карусели
            loop: false,     // Бесконечное зацикливание карусели
            auto: true,     // Автоматическая прокрутка
            interval: 5000, // Интервал между прокруткой элементов (мс)
            speed: 750,     // Скорость анимации (мс)
            touch: false,    // Прокрутка  прикосновением
            arrows: true,   // Прокрутка стрелками
            dots: true      // Индикаторные точки
        };

        this.initialize();

        for (let i = 0; i < this.images.length; i++) {
            this.stages.push(this.elemWidth * i);
        }
    }


    stages = []
    elemPrev = (num) => {
        num = num || 1;
        this.gotoElem(this.currentElement - num < 0
            ? this.images.length - (Math.abs(this.currentElement - num)) % this.images.length
            : this.currentElement - num); //diff
    }

    elemNext = (num) => {
        num = num || 1;
        this.gotoElem((this.currentElement + num) % this.images.length);
    }

    gotoElem = (num) => {
        if (this.options.dots)
            this.dotOn(this.currentElement);
        this.currentElement = num;
        if (this.options.dots)
            this.dotOff(this.currentElement);
        this.crslList.scrollLeft = this.stages[this.currentElement];
    }

    dotOn = (num) => {
        console.log(num)
        this.indicatorDotsAll[num].style.cssText = 'background-color:#BBB; cursor:pointer;'
    }

    dotOff = (num) => {
        console.log(num)
        this.indicatorDotsAll[num].style.cssText = 'background-color:#556; cursor:default;'
    }

    getTime() {
        return new Date().getTime();
    }

    setAutoScroll = () => {
        this.autoScroll = setInterval(() => {
            let fnTime = this.getTime();
            if (fnTime - this.bgTime + 10 > this.options.interval) {
                this.bgTime = fnTime;
                this.elemNext()
            }
        }, this.options.interval);
    }

    initialize() {
        // Constants
        this.elemCount = this.crslElements.length; // Количество элементов
        this.dotsVisible = this.elemCount;         // Число видимых точек
        let elemStyle = window.getComputedStyle(this.crslElemFirst);
        this.elemWidth = this.crslElemFirst.offsetWidth +  // Ширина элемента (без margin)
            parseInt(elemStyle.marginLeft) + parseInt(elemStyle.marginRight);

        // Variables
        this.currentElement = 0;
        this.currentOffset = 0;
        this.touchPrev = true;
        this.touchNext = true;
        this.bgTime = this.getTime();

        // Functions

        // Start initialization
        if (this.elemCount <= this.options.elemVisible) {   // Отключить навигацию
            this.options.auto = false;
            this.options.touch = false;
            this.options.arrows = false;
            this.options.dots = false;
            this.leftArrow.style.display = 'none';
            this.rightArrow.style.display = 'none';
        }

        this.crslList.addEventListener('wheel', (e) => {
            if (e.deltaX === 0) return;
            e.preventDefault();
            e.stopPropagation();
        }, {passive: false});


        if (!this.options.loop) {       // если нет цикла - уточнить количество точек
            this.dotsVisible = this.elemCount - this.options.elemVisible + 1;
            this.touchPrev = false;    // отключить прокрутку прикосновением вправо
            this.options.auto = false; // отключить автопркрутку
        } else if (this.options.auto) {   // инициализация автопрокруки
            this.setAutoScroll();
            // Остановка прокрутки при наведении мыши на элемент
            this.crslList.addEventListener('mouseenter', function () {
                clearInterval(this.autoScroll)
            }, false);
            this.crslList.addEventListener('mouseleave', this.setAutoScroll, false);
        }

        if (this.options.arrows) // инициализация стрелок
            this.initializeArrows();
        else
            this.hideArrows();

        if (this.options.dots)
            this.initializeIndicatorDots();

        this.initializeSwipes();
    }

    hideArrows() {
        this.leftArrow.style.display = 'none';
        this.rightArrow.style.display = 'none';
    }

    initializeArrows() {
        if (!this.options.loop)
            this.crslList.style.cssText = 'transition:margin ' + this.options.speed + 'ms ease;';
        this.leftArrow.addEventListener('click', this.leftArrowClickEvent, false);
        this.rightArrow.addEventListener('click', this.rightArrowClickEvent, false)
    }

    indicatorDotsAll
    initializeIndicatorDots = () => {
        let sum = '';
        for (let i = 0; i < this.dotsVisible; i++) {
            sum += '<span class="dot"></span>'
        }

        this.indicatorDots.innerHTML = sum;
        this.indicatorDotsAll = this.crslRoot.querySelectorAll('.dot');
        // Назначаем точкам обработчик события 'click'

        for (let n = 0; n < this.dotsVisible; n++) {
            this.indicatorDotsAll[n].addEventListener('click', () => this.dotsClickEvent(n), false)
        }

        this.dotOff(0);  // точка[0] выключена, остальные включены
        for (let i = 1; i < this.dotsVisible; i++) {
            this.dotOn(i)
        }
    }

    initializeSwipes() {
        let xTouch, yTouch, xDiff, yDiff, stTime, mvTime;
        this.crslList.addEventListener('touchstart', (e) => {
            xTouch = parseInt(e.touches[0].clientX);
            yTouch = parseInt(e.touches[0].clientY);
            stTime = this.getTime()
        }, false);
        this.crslList.addEventListener('touchmove', (e) => {
            e.preventDefault();
            if (!xTouch || !yTouch) return;
            xDiff = xTouch - parseInt(e.touches[0].clientX);
            yDiff = yTouch - parseInt(e.touches[0].clientY);
            mvTime = this.getTime();
            if (Math.abs(xDiff) > 10 && Math.abs(xDiff) > Math.abs(yDiff) && mvTime - stTime < 75) {
                stTime = 0;
                if (xDiff > 0) {
                    this.bgTime = mvTime;
                    this.elemNext()
                } else if (xDiff < 0) {
                    this.bgTime = mvTime;
                    this.elemPrev()
                }
            }
        }, false);
    }

    leftArrowClickEvent = () => {
        this.arrowClickEvent(this.elemPrev)
    }

    rightArrowClickEvent = () => {
        this.arrowClickEvent(this.elemNext)
    }

    arrowClickEvent = (nextOrPrev) => {
        nextOrPrev();
    }
    dotsClickEvent = (n) => {
        this.gotoElem(n);
    }
}
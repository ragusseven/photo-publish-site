import {DOMElementsRemover} from "../../../common/DOMElementsRemover";

export class inputFieldValidation {
    constructor(validationDelegate, message) {
        this.validation = validationDelegate;
        this.message = message;
        this.byBlurIgnored = false;
    }
}

export class InputField extends HTMLElement {
    constructor() {
        super();
        this.#setLabel();
        this.setField();
    }

    validations = [];

    get value() {
        return this.field().value;
    }

    set value(value) {
        this.field().value = value;
    }

    get disabled() {
        return this.field().disabled;
    }

    set disabled(value) {
        this.field().disabled = value;
    }

    field() {
        return this.querySelector('.data-input-field');
    }

    #setLabel() {
        let label = document.createElement('label');
        label.setAttribute('for', this.getAttribute('id') + '_field');
        label.textContent = this.getAttribute('text');
        label.setAttribute('class', 'data-input-label');
        this.appendChild(label);
    }

    setField() {
        let field = document.createElement('input');
        field.setAttribute('type', this.getAttribute('type') || 'text');
        field.setAttribute('name', this.getAttribute('name'));
        field.setAttribute('id', this.getAttribute('id') + '_field');
        field.setAttribute('class', 'data-input-field');
        field.setAttribute('placeholder', this.getAttribute('placeholder')||' ');
        this.appendChild(field);
    }

    invalidateField(message) {
        this.validateField();
        let text = document.createElement('div');
        text.setAttribute('class', 'invalid-field-message');
        text.textContent = message;
        this.lastElementChild.setCustomValidity(message);
        this.appendChild(text);
    }

    validateField() {
        let remover = new DOMElementsRemover()
        let elements = this.querySelectorAll('.invalid-field-message');
        for (let element of elements) {
            remover.remove(element);
        }
        remover.execute();
        this.lastElementChild.setCustomValidity('');
    }

    async checkValidity(blur) {
        for (let check of (blur ? this.validations.filter((validation) => !validation.byBlurIgnored) : this.validations)) {
            if (!await check.validation(this.field().value)) {
                this.invalidateField(check.message);
                return false;
            }
        }
        this.validateField();
        return true;
    }

    setCheckOnBlur() {
        this.field().addEventListener('blur', () => this.checkValidity(true));
        return this;
    }
}
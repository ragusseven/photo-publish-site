import {InputField} from "./InputField";

export class TextAreaInputField extends InputField {
    constructor(props) {
        super(props);
    }

    setField() {
        let field = document.createElement('textarea');
        field.setAttribute('type', this.getAttribute('type'));
        field.setAttribute('name', this.getAttribute('name'));
        field.setAttribute('id', this.getAttribute('id') + '_field');
        field.setAttribute('class', 'data-input-field');
        field.setAttribute('placeholder', this.getAttribute('placeholder')||' ');
        this.appendChild(field);
    }
}
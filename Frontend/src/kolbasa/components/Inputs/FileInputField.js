import {InputField} from "./InputField";

export class FileInputField extends InputField {
    constructor(props) {
        super(props);
    }

    setField() {
        let field = document.createElement('drag-and-drop-image-form');
        field.setAttribute('type', this.getAttribute('type'));
        if (this.getAttribute('multi'))
            field.setAttribute('multi', this.getAttribute('multi'));
        field.setAttribute('name', this.getAttribute('name'));
        field.setAttribute('id', this.getAttribute('id') + '_field');
        field.setAttribute('class', 'file-dnd-style');
        field.setAttribute('target', this.getAttribute('target'));
        this.appendChild(field);
    }

    get images() {
        return this.field().images;
    }

    set images(value) {
        this.field().images = value;
    }

    field() {
        return this.querySelector('.file-dnd-style');
    }

    displayImages() {
        this.field().displayImages();
    }

    async checkValidity() {
        for (let check of this.validations) {
            if (!await check.validation(this.images)) {
                this.invalidateField(check.message);
                return false;
            }
        }
        this.validateField();
        return true;
    }
}
import {fillHtmlTemplateFromJson} from "../common";

export class DragAndDropImageForm extends HTMLElement {
    images = [];
    input;
    button;
    preview;
    multi = false;

    connectedCallback() {
        let options = {
            multi: this.getAttribute('multi'),
            accept: ['.png', '.jpg', '.jpeg', '.gif']
        }
        this.innerHTML = ` <div class="dnd-text">Перетащите ${this.getAttribute('target')}<br>  или</div><button class=\"accent-button\">Выберите</button>\n` +
        `        <input type=\"file\" id=${this.getAttribute('id') + '_input'} name=${this.getAttribute('name')} accept=\"image/*\" required `
            +(options.multi ? 'multiple' : '') + `>`

        this.input = this.querySelector('input');
        this.input.setAttribute('placeholder', this.getAttribute('target'));
        this.button = this.querySelector('button');
        this.preview = document.createElement("div");
        this.preview.classList.add('preview');
        this.preview.style.display = 'none';
        if (options.multi) {
            this.multi = options.multi;
        }
        if (options.accept && Array.isArray(options.accept)) {
            this.input.setAttribute('accept', options.accept.join(','))
        }
        this.input.insertAdjacentElement('afterend', this.preview)

        function preventDefaults(e) {
            e.preventDefault()
            e.stopPropagation()
        }

        ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
            this.addEventListener(eventName, preventDefaults, false)
            document.body.addEventListener(eventName, preventDefaults, false)
        });

        this.button.addEventListener('click', () => this.input.click());
        this.addEventListener('drop', (e) => {
            e.preventDefault();
            const files = e.dataTransfer.files;
            if (!this.multi && files.length === 1 || this.multi) {
                if (!this.multi) {
                    this.images = [];
                }
                this.pushFiles(files);
                this.displayImages();
            }
        });

        this.input.addEventListener('change', (e) => {
            const files = Array.from(this.input.files)
            if (!this.multi && files.length === 1 || this.multi) {
                if (!this.multi) {
                    this.images = [];
                }
                this.pushFiles(files);
                this.displayImages();
            }
        });
        this.preview.addEventListener('click', this.removePhoto);
    }

    removePhoto = event => {
        if (!event.target.dataset.name) {
            return;
        }

        const {name} = event.target.dataset;
        this.images = this.images.filter(file => file.name !== name);

        const block = this.preview.querySelector(`[data-name="${name}"]`)
            .closest('.preview-image');

        block.remove();

        if (this.images.length === 0)
            this.preview.style.display = 'none';
    }


    displayImages() {
        this.preview.style.display = '';
        this.preview.innerHTML = ''
        this.images.forEach(file => {
            this.#appendFileImage(file);
        });
        if (this.images.length === 0)
            this.preview.style.display = 'none';
    }

    #appendFileImage(file) {
        const reader = new FileReader();

        reader.onload = ev => {
            const src = ev.target.result;
            const html = `<div class="preview-image">` +
                `<div class="preview-remove" data-name="${file.name}"><img src="/svg/close-button.svg" alt="&times"  data-name="${file.name}" class="close-button-ico"></div>` +
                `<img class="preview-image-thumbnail" src="${src}" alt="${file.name}" />` +
                `</div></div>`;
            this.preview.insertAdjacentHTML('afterbegin', html);
        }
        reader.readAsDataURL(file);
    }

    pushFiles(files) {
        for (let i = 0; i < files.length; i++) {
            if (!files[i].type.match("image")) {
                continue;
            }

            if (this.images.every(image => image.name !== files[i].name)) {
                this.images.push(files[i]);
            }
        }
    }

    get disabled() {
        return this.button.disabled;
    }

    set disabled(value) {
        this.button.disabled = value;
        this.input.disabled = value;
    }

    setCustomValidity(value) {
        this.input.setCustomValidity(value);
    }
}
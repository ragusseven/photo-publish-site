// возвращает куки с указанным name,
// или undefined, если ничего не найдено
export function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

export function setCookie(name, value, options = {}) {

    options = {
        path: '/',
        // при необходимости добавьте другие значения по умолчанию
        ...options
    };

    if (options.expires instanceof Date) {
        options.expires = options.expires.toUTCString();
    }

    let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

    for (let optionKey in options) {
        updatedCookie += "; " + optionKey;
        let optionValue = options[optionKey];
        if (optionValue !== true) {
            updatedCookie += "=" + optionValue;
        }
    }

    document.cookie = updatedCookie;
}

export function deleteCookie(name) {
    setCookie(name, "", {
        'max-age': -1
    })
}

export function objectToQueryParams(object) {
    let paramsString = [];
    for (let i in object) {
        paramsString.push(`${i}=${object[i]}`)
    }
    return paramsString.join('&');
}

export function queryParamsToObject(params) {
    if (!params) return {};
    let paramsString = params.split('&');
    let obj = {};
    for (let i of paramsString) {
        let keyValue = i.split('=');
        obj[keyValue[0]] = keyValue[1];
    }
    return obj;
}

export function getDocumentHeight() {
    return Math.max(
        document.documentElement.scrollHeight,
        document.documentElement.offsetHeight,
        document.body.scrollHeight,
        document.body.offsetHeight
    );
}

export function fillHtmlTemplateFromJson(content, authorInfo) {
    let fillableFields = content.matchAll('{.*?}');
    for (let fillableField of fillableFields) {
        content = content.replaceAll(fillableField[0], authorInfo[fillableField[0].slice(1, -1)] || 'undefined');
    }
    let template = document.createElement('template');
    template.innerHTML = content;
    return template.content;
}

export function convertToFormData(object) {
    let data = new FormData();
    for (let i in object) {
        data.append(i, object[i]);
    }
    return data;
}

export async function FetchAndHandleWithRetry(url, options = null, addAuthorizationCookie = true, attemptsNumber = 5) {
    let finalOptions = options;
    if (addAuthorizationCookie) {
        const accessToken = getCookie("accessToken");
        if (accessToken !== undefined)
            finalOptions = Object.assign({}, options, {headers: {"Authorization": "Bearer " + accessToken}});
    }
    let response = "";
    for (let i = 0; i < attemptsNumber; i++) {
        try {
            response = await fetch(url, finalOptions);
            if (response.status < 500 || response.status >= 600) {
                return response;
            } else {
                if (i === attemptsNumber - 1) {
                    return {ok: false, status: response.status};
                }
            }
        } catch (e) {

        }
        await sleep(Math.pow(1.5, i) * 1000)
    }
    if (response === "" || response.status === undefined)
        console.error("Network error");
    else {
        const error = `${response.status} ${response.statusText}`;
        console.error(error);
    }
    return {ok: false};
}


export function sleep(ms) {
    // add ms millisecond timeout before promise resolution
    return new Promise(resolve => setTimeout(resolve, ms))
}

export function getFullHeight(element) {
    const computedStyle = window.getComputedStyle(element);
    const marginTop = parseInt(computedStyle.marginTop, 10);
    const marginBottom = parseInt(computedStyle.marginBottom, 10);
    const offsetHeight = element.offsetHeight;
    return offsetHeight + marginTop + marginBottom;
}

export function flattenObject(obj, delimiter = '.', prefix = '') {
    const result = {};
    for (const prop in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, prop)) {
            const key = prefix ? prefix + delimiter + prop : prop;
            const value = obj[prop];
            if (typeof value === 'object' && value !== null) {
                Object.assign(result, flattenObject(value, key));
            } else {
                result[key] = value;
            }
        }
    }
    return result;
}

export function srcToFile(src, fileName, mimeType) {
    return (fetch(src)
            .then(function (res) {
                return res.arrayBuffer();
            })
            .then(function (buf) {
                return new File([buf], fileName, {type: mimeType});
            })
    );
}
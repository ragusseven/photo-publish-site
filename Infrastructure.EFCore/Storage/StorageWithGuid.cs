﻿using System.Linq.Expressions;
using Infrastructure.EFCore.Dbo;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.EFCore.Storage;

public abstract class StorageWithGuid<T, TDbContext> : IStorage<T, Guid>
    where T : class, IEntity<Guid>
    where TDbContext : DbContext
{
    private bool _disposed;
    protected readonly TDbContext Context;

    protected StorageWithGuid(TDbContext context)
    {
        Context = context;
    }

    public virtual IEnumerable<T> ReadAll()
    {
        return Context.Set<T>().ToArray();
    }

    public virtual async Task<IEnumerable<T>> ReadAllASync()
    {
        return await Context.Set<T>().ToListAsync();
    }

    public virtual T? Read(Guid id)
    {
        return Context.Set<T>().FirstOrDefault(u => u.Id == id);
    }

    public virtual async Task<T?> ReadAsync(Guid id)
    {
        return await Context.Set<T>().FirstOrDefaultAsync(u => u.Id == id);
    }

    public virtual IEnumerable<T> Read(Expression<Func<T, bool>> lambda)
    {
        return Context.Set<T>().Where(lambda).ToArray();
    }

    public virtual async Task<IEnumerable<T>> ReadAsync(Expression<Func<T, bool>> lambda)
    {
        return await Context.Set<T>().Where(lambda).ToArrayAsync();
    }

    public virtual Guid Create(T item)
    {
        Context.Set<T>().Add(item);
        return item.Id;
    }

    public virtual async Task<Guid> CreateAsync(T item)
    {
        await Context.Set<T>().AddAsync(item);
        return item.Id;
    }


    public virtual void Update(T item)
    {
        Context.Set<T>().Update(item).State = EntityState.Modified;
    }

    public virtual Task UpdateAsync(T item)
    {
        Context.Set<T>().Update(item).State = EntityState.Modified;
        return Task.CompletedTask;
    }

    public virtual void Delete(Guid id)
    {
        var user = Read(id);
        if (user != null)
        {
            Context.Set<T>().Remove(user);
        }
    }

    public virtual async Task DeleteAsync(Guid id)
    {
        var user = await ReadAsync(id);
        if (user != null)
        {
            Context.Set<T>().Remove(user);
        }
    }


    public virtual void Save()
    {
        Context.SaveChanges();
    }

    public virtual async Task SaveAsync()
    {
        await Context.SaveChangesAsync();
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!_disposed && disposing)
        {
            try
            {
                Save();
                Context.Dispose();
            }
            catch (ObjectDisposedException)
            {
            }
        }

        _disposed = true;
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }
}
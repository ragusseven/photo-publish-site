using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.EFCore.DIExtension;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddFactory<TImplementation>(this IServiceCollection services)
        where TImplementation : class
    {
        services.AddTransient<TImplementation>();
        
        services.AddSingleton<Func<TImplementation>>(x => () => x.GetService<TImplementation>()!);
        services.AddSingleton<IFactory<TImplementation>, Factory<TImplementation>>();
        
        return services;
    }

    public static IServiceCollection AddFactory<TService, TImplementation>(this IServiceCollection services)
        where TService : class
        where TImplementation : class, TService
    {
        services.AddTransient<TService, TImplementation>();
        services.AddSingleton<Func<TService>>(x => () => x.GetService<TService>()!);
        services.AddSingleton<IFactory<TService>, Factory<TService>>();
        
        return services;
    }
}
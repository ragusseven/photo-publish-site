namespace PhotoProject.Infrastructure.Common;

public class Result<T>
{
    public T? Value;
    public bool IsSuccess => Value is not null;
    public string? ErrorMessage;

    public static implicit operator Result<T>(T value)
        => new()
        {
            Value = value
        };
    

    public static implicit operator Result<T>(Result result)
    {
        return new Result<T>()
        {
            ErrorMessage = result.Message
        };
    }
}

public class Result
{
    public string? Message;
    public static Result Fail(string message)
    {
        return new Result()
        {
            Message = message
        };
    }
}
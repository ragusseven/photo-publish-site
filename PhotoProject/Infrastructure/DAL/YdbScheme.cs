namespace PhotoProject.Infrastructure.DAL;

public class YdbScheme : IYdbScheme
{
    public string Photosession => "photosessions";
    public string Image => "images";
    public string Publication => "publications";
    public string Record => "records";
    public string User => "users";
}
﻿using PhotoProject.Infrastructure.Common;
using WebApi.Infrastructure.Attributes;
using Ydb.Sdk.Value;

namespace PhotoProject.Infrastructure.DAL.Models;

public class ImageDbo
{
    [Column("id", YdbType.Guid)] public Guid Id { get; set; }
    [Column("image", YdbType.Bytes)] public byte[] Image { get; set; }
}

public static class ImageExtentions
{
    private static void DontCallMe()
    {
        new ImageDbo().GetProperties();
    }

    public static ImageDbo ImageFromYdb(this ResultSet.Row row)
    {
        return new ImageDbo
        {
            Id = row["id"].GetOptionalString().AsGuid(),
            Image = row["image"].GetOptionalString(),
        };
    }
}
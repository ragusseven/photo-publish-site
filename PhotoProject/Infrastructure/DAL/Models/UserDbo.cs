﻿using System.Text;
using PhotoProject.Infrastructure.Common;
using WebApi.Infrastructure.Attributes;
using Ydb.Sdk.Value;

// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global
#pragma warning disable CS8618
namespace PhotoProject.Infrastructure.DAL.Models;

public class UserDbo
{
    [Column("user_id", YdbType.Guid)] public Guid Id { get; set; }
    [Column("login", YdbType.String)] public string Login { get; set; }
    [Column("email", YdbType.String)] public string Email { get; set; }

    [Column("profile_name", YdbType.String)]
    public string ProfileName { get; set; }

    [Column("password_hash", YdbType.String)]
    public string PasswordHash { get; set; }

    [Column("name", YdbType.OptionalString)]
    public string? Name { get; set; }

    [Column("surname", YdbType.OptionalString)]
    public string? Surname { get; set; }

    [Column("photo", YdbType.OptionalString)]
    public string? Photo { get; set; }
}

public static class UserExtensions
{
    private static void DontCallMe()
    {
        new UserDbo().GetProperties();
    }

    public static UserDbo UserFromYdb(this ResultSet.Row row)
    {
        return new UserDbo
        {
            Id = row["user_id"]
                 .GetString()
                 .AsGuid(),
            Login = Encoding.Default.GetString(row["login"]
                .GetString()),
            Email = Encoding.Default.GetString(row["email"]
                .GetString()),
            ProfileName = Encoding.Default.GetString(row["profile_name"]
                .GetString()),
            PasswordHash = Encoding.Default.GetString(row["password_hash"]
                .GetString()),
            Name = row["name"].GetOptionalString().AsNullableString(),
            Surname = row["surname"].GetOptionalString().AsNullableString(),
            Photo = row["photo"].GetOptionalString().AsNullableString(),
        };
    }
}

public record UserExtraFields(string? Name, string? Surname, string? Photo);
﻿using System.Text;
using PhotoProject.Infrastructure.Common;
using WebApi.Infrastructure.Attributes;
using Ydb.Sdk.Value;

namespace PhotoProject.Infrastructure.DAL.Models;

public class PhotoSessionDbo
{
    [Column("id", YdbType.Guid)]
    public Guid Id { get; set; }
    [Column("name", YdbType.String)]
    public string Name { get; set; }
    [Column("price", YdbType.Int)]
    public int Price { get; set; }
    [Column("start_date", YdbType.DateTime)]
    public DateTime StartDate { get; set; }
    [Column("end_date", YdbType.DateTime)]
    public DateTime EndDate { get; set; }
    [Column("status", YdbType.Int)]
    public Status Status { get; set; }
    [Column("user_id", YdbType.Guid)]
    public Guid UserId { get; set; }
}

public static class PhotosessionDboExtensions
{
    private static void DontCallMe()
    {
        new PhotoSessionDbo().GetProperties();
    }
    public static PhotoSessionDbo FromYRow(ResultSet.Row row)
    {
        return new PhotoSessionDbo
        {
            Id = row["id"].GetString().GetGuid(),
            Name = Encoding.Default.GetString(row["name"].GetString()),
            Price = row["price"].GetInt32(),
            StartDate = row["start_date"].GetDatetime(),
            EndDate = row["end_date"].GetDatetime(),
            Status = (Status) row["status"].GetInt32(),
            UserId = row["userId"].GetString().GetGuid()
        };
    }
}
public enum Status
{
    Unknown = 0,
    Open = 1,
    Closed = 2
}
﻿using System.Text;
using PhotoProject.Infrastructure.Common;
using WebApi.Infrastructure.Attributes;
using Ydb.Sdk.Value;

namespace PhotoProject.Infrastructure.DAL.Models;

public class PublicationDbo
{
    [Column("publication_id", YdbType.Guid)]
    public Guid Id { get; set; }
    [Column("publication_header", YdbType.String)]
    public string Header { get; set; }
    [Column("publication_text", YdbType.OptionalString)]
    public string? Text { get; set; }
    [Column("publication_preview", YdbType.String)]
    public string Preview { get; set; }
    [Column("publication_images_id", YdbType.String)]
    public string ImagesId { get; set; }
    [Column("author_id", YdbType.Guid)]
    public Guid UserId { get; set; }
    [Column("publication_time", YdbType.DateTime)]
    public DateTime DateTime { get; set; }
}

public static class PublicationExtensions
{
    private static void DontCallMe()
    {
        new PublicationDbo().GetProperties();
    }
    public static PublicationDbo PublicationFromYdb(this ResultSet.Row row)
    {
        return new PublicationDbo
        {
            Id = row["publication_id"].GetOptionalString()!.AsGuid(),
            Header = Encoding.Default.GetString(row["publication_header"].GetOptionalString() ?? Array.Empty<byte>()),
            Text = Encoding.Default.GetString(row["publication_text"].GetOptionalString() ?? Array.Empty<byte>()),
            Preview = Encoding.Default.GetString(row["publication_preview"].GetOptionalString() ?? Array.Empty<byte>()),
            ImagesId = Encoding.Default.GetString(row["publication_images_id"].GetOptionalString() ?? Array.Empty<byte>()),
            UserId = row["author_id"].GetOptionalString()!.AsGuid(),
            DateTime = row["publication_time"].GetOptionalDatetime()!.Value,
        };
    }
}
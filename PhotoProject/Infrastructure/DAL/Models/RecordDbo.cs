﻿using System.Text;
using PhotoProject.Infrastructure.Common;
using WebApi.Infrastructure.Attributes;
using Ydb.Sdk.Value;

namespace PhotoProject.Infrastructure.DAL.Models;

public class RecordDbo
{
    [Column("record_id", YdbType.Guid)]
    public Guid Id { get; set; }
    [Column("record_status", YdbType.Int)]
    public Status Status { get; set; }
    [Column("recipient_email", YdbType.String)]
    public string Email { get; set; }
    [Column("recipient_name", YdbType.OptionalString)]
    public string? Name { get; set; }
    [Column("ph_dbo_id", YdbType.Guid)]
    public Guid PhotoSessionDboId { get; set; }
}
public static class RecordDboExt
{
    private static void DontCallMe()
    {
        new RecordDbo().GetProperties();
    }
    public static RecordDbo RecordFromYdb(this ResultSet.Row row)
    {
        return new RecordDbo
        {
            Id = row["record_id"]
                 .GetOptionalString()!
                 .AsGuid(),
            Status = (Status)row["record_status"].GetOptionalInt32()!.Value,
            Email = Encoding.Default.GetString(row["recipient_email"]
                .GetOptionalString() ?? Array.Empty<byte>()),
            Name = row["recipient_name"].GetOptionalString().AsNullableString(),
            PhotoSessionDboId = row["ph_dbo_id"]
                                .GetOptionalString()!
                                .AsGuid(),
        };
    }
}
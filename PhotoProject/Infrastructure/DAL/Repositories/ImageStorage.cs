﻿using PhotoProject.Infrastructure.Common;
using PhotoProject.Infrastructure.DAL.Models;
using Ydb.Sdk.Value;

namespace PhotoProject.Infrastructure.DAL.Repositories;

/// <summary>
/// Картинки
/// </summary>
public class ImageStorage : BaseStorage<ImageDbo>
{
    private string _select => "SELECT id, image";

    /// <summary>
    /// Картинки
    /// </summary>
    /// <param name="context"></param>
    public Task CreateAsync(ImageDbo dbo)
    {
        var ydb = _serializerHelper.GenerateInsert(dbo, _scheme.Image);
        return _client.ExecuteModify(ydb.Yql, ydb.Item2);
    }
    public Task UpdateAsync(ImageDbo dbo)
    {
        var ydb = _serializerHelper.GenerateUpdate(dbo, _scheme.Image);
        return _client.ExecuteModify(ydb.Yql, ydb.Item2);
    }
    
    public async Task DeleteAsync(Guid id)
    {
        await _client.ExecuteModify($@"
            DECLARE $id AS string;

            DELETE FROM {_scheme.Image}
            WHERE id = $id
        ", new Dictionary<string, YdbValue>
        {
            {"$id", YdbValue.MakeString(id.GetStringView())},
        });
    }
    public async Task<ImageDbo?> GetImageById(Guid id)
    {
        var rows = await _client.ExecuteFind($@"
            DECLARE $id AS string;

            {_select}
            FROM {_scheme.Image}
            WHERE id = $id
        ", new Dictionary<string, YdbValue>
        {
            {"$id", YdbValue.MakeString(id.GetStringView())},
        });

        if (rows is null) return null;
        return rows.Single().ImageFromYdb();
    }

    public ImageStorage(IYdbScheme scheme, YdbSerializerHelper<ImageDbo> serializerHelper, IYdbClient client) 
        : base(scheme, serializerHelper, client)
    {
    }
}
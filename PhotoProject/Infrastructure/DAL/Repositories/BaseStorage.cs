namespace PhotoProject.Infrastructure.DAL.Repositories;

public class BaseStorage<T>
{
    protected readonly IYdbScheme _scheme;
    protected readonly YdbSerializerHelper<T> _serializerHelper;
    protected readonly IYdbClient _client;

    public BaseStorage(IYdbScheme scheme, YdbSerializerHelper<T> serializerHelper, IYdbClient client)
    {
        _scheme = scheme;
        _serializerHelper = serializerHelper;
        _client = client;
    }
}
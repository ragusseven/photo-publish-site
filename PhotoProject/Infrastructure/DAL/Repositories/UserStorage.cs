﻿using System.Text;
using PhotoProject.Infrastructure.Common;
using PhotoProject.Infrastructure.DAL.Models;
using Ydb.Sdk.Value;

namespace PhotoProject.Infrastructure.DAL.Repositories;

/// <summary>
/// Пользователи
/// </summary>
public class UserStorage
{
    private readonly IYdbClient _client;
    private readonly IYdbScheme _scheme;
    private readonly YdbSerializerHelper<UserDbo> _serializerHelper;

    /// <summary>
    /// Пользователи
    /// </summary>
    /// <param name="client"></param>
    /// <param name="scheme"></param>
    public UserStorage(IYdbClient client, IYdbScheme scheme, YdbSerializerHelper<UserDbo> serializerHelper)
    {
        _client = client;
        _scheme = scheme;
        _serializerHelper = serializerHelper;
    }

    /// <summary>
    /// Получить информацию о пользователе по логину
    /// </summary>
    /// <param name="login">Логин пользователя</param>
    /// <returns></returns>
    public async Task<UserDbo?> GetByLogin(string login)
    {
        var rows = await _client.ExecuteFind($"""
                                              
                                                          DECLARE $id AS string;
                                              
                                                          {Select}
                                                          FROM {_scheme.User}
                                                          WHERE login = $id
                                                      
                                              """, new Dictionary<string, YdbValue>
        {
            {"$id", YdbValue.MakeString(Encoding.Default.GetBytes(login))},
        });

        return rows?.FirstOrDefault()?.UserFromYdb();
    }

    public async Task<UserDbo?> GetById(Guid id)
    {
        var rows = await _client.ExecuteFind($"""
                                              
                                                          DECLARE $id AS string;
                                              
                                                          {Select}
                                                          FROM {_scheme.User}
                                                          WHERE user_id = $id
                                                      
                                              """, new Dictionary<string, YdbValue>
        {
            {"$id", YdbValue.MakeString(id.GetStringView())},
        });

        return rows?.Single().UserFromYdb();
    }
    
    public async Task CreateAsync(UserDbo dbo)
    {
        var ydb = _serializerHelper.GenerateInsert(dbo, _scheme.User);
        await _client.ExecuteModify(ydb.Yql, ydb.Item2);
    }

    private static string Select => "SELECT user_id, login, email, profile_name, password_hash, name, surname, photo";
}
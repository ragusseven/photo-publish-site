﻿using PhotoProject.Infrastructure.Common;
using PhotoProject.Infrastructure.DAL.Models;
using Ydb.Sdk.Value;

namespace PhotoProject.Infrastructure.DAL.Repositories;

/// <summary>
/// Записи на фотосессию
/// </summary>
public class RecordStorage : BaseStorage<RecordDbo>
{
    /// <summary>
    /// Получить запись на фотосессию по Id
    /// </summary>
    /// <param name="id">Id публикации</param>
    /// <returns></returns>
    public async Task<RecordDbo?> GetRecord(Guid id)
    {
        var rows = await _client.ExecuteFind($"""
                                              
                                                          DECLARE $id AS string;
                                              
                                                          {_select}
                                                          FROM {_scheme.Record}
                                                          WHERE record_id = $id
                                                      
                                              """, new Dictionary<string, YdbValue>
        {
            {"$id", YdbValue.MakeString(id.GetStringView())},
        });

        if (rows is null) return null;
        return rows.Single().RecordFromYdb();
    }

    public async Task DeleteAsync(Guid id)
    {
        await _client.ExecuteModify($"""
                                     DECLARE $id AS string;

                                     DELETE
                                     FROM {_scheme.Record}
                                     WHERE record_id = $id
                                     """, new Dictionary<string, YdbValue>
        {
            {"$id", YdbValue.MakeString(id.GetStringView())},
        });
    }

    public Task CreateAsync(RecordDbo dbo)
    {
        var ydb = _serializerHelper.GenerateInsert(dbo, _scheme.Record);
        return _client.ExecuteModify(ydb.Yql, ydb.Item2);
    }

    public async Task DeleteAllExceptOne(Guid photoSessionId, Guid id)
    {
        await _client.ExecuteModify($"""
                                     DECLARE $id AS string;
                                     DECLARE $ph_id AS string;

                                     DELETE
                                     FROM {_scheme.Record}
                                     WHERE ph_dbo_id = $ph_id
                                     AND record_id != $id
                                     """, new Dictionary<string, YdbValue>
        {
            {"$id", YdbValue.MakeString(id.GetStringView())},
            {"$ph_id", YdbValue.MakeString(photoSessionId.GetStringView())}
        });
    }

    public async Task<IEnumerable<RecordDbo>> GetRecordByPhotoSession(Guid photoSessionId)
    {
        var rows = await _client.ExecuteFind($"""
                                              
                                                          DECLARE $id AS string;
                                              
                                                          {_select}
                                                          FROM {_scheme.Record}
                                                          WHERE ph_dbo_id = $id
                                                      
                                              """, new Dictionary<string, YdbValue>
        {
            {"$id", YdbValue.MakeString(photoSessionId.GetStringView())},
        });

        if (rows is null) return null;
        return rows.Select(x => x.RecordFromYdb());
    }

    private string _select => "SELECT record_id, record_status, recipient_email, recipient_name, ph_dbo_id";

    public RecordStorage(IYdbScheme scheme, YdbSerializerHelper<RecordDbo> serializerHelper, IYdbClient client) : base(
        scheme, serializerHelper, client)
    {
    }
}
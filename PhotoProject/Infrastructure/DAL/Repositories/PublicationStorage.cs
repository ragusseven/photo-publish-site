﻿using System.Text;
using PhotoProject.Infrastructure.Common;
using PhotoProject.Infrastructure.DAL.Models;
using Ydb.Sdk.Value;

namespace PhotoProject.Infrastructure.DAL.Repositories;

/// <summary>
/// Публикации
/// </summary>
public class PublicationStorage : BaseStorage<PublicationDbo>
{
    /// <summary>
    /// Публикации
    /// </summary>
    /// <param name="context"></param>
    /// <summary>
    /// Получить публикацию по Id
    /// </summary>
    /// <param name="id">Id публикации</param>
    /// <returns></returns>
    public async Task<PublicationDbo?> GetPublication(Guid id)
    {
        var rows = await _client.ExecuteFind($@"
            DECLARE $id AS string;

            {_select}
            FROM {_scheme.Publication}
            WHERE publication_id = $id
        ", new Dictionary<string, YdbValue>
        {
            {"$id", YdbValue.MakeString(id.GetStringView())},
        });

        if (rows is null) return null;
        return rows.Single().PublicationFromYdb();
    }

    /// <summary>
    /// Получить все публикации по Id юзера
    /// </summary>
    /// <param name="userId">Id юзера</param>
    /// <returns></returns>
    public async Task<IEnumerable<PublicationDbo>> GetUserPublications(Guid userId)
    {
        var rows = await _client.ExecuteFind($@"
            DECLARE $id AS string;

            {_select}
            FROM {_scheme.Publication}
            WHERE author_id = $id
        ", new Dictionary<string, YdbValue>
        {
            {"$id", YdbValue.MakeString(userId.GetStringView())},
        });

        return rows is null ? Array.Empty<PublicationDbo>() : rows.Select(x => x.PublicationFromYdb());
    }

    public Task CreateAsync(PublicationDbo dbo)
    {
        var ydb = _serializerHelper.GenerateInsert(dbo, _scheme.Publication);
        return _client.ExecuteModify(ydb.Yql, ydb.Item2);
    }

    public async Task DeleteAsync(Guid userId)
    {
        await _client.ExecuteModify($"""
                                                 DELETE
                                                 FROM {_scheme.Publication}
                                                 WHERE author_id = $id
                                     """, new Dictionary<string, YdbValue>
        {
            {"$id", YdbValue.MakeString(userId.GetStringView())},
        });
    }

    /// <summary>
    /// Получить пагинированный список публикаций
    /// </summary>
    /// <param name="pageNumber">Номер страницы</param>
    /// <returns></returns>
    public async Task<IEnumerable<PublicationDbo>> GetPublicationsByPage(int pageNumber, int pageSize)
    {
        var rows = await _client.ExecuteFind($"""
                                                          {_select}
                                                          FROM {_scheme.Publication}
                                                          ORDER BY publication_time
                                                          LIMIT {pageSize} OFFSET {(pageNumber - 1) * pageSize}
                                              """, new Dictionary<string, YdbValue>());

        return rows is null ? Array.Empty<PublicationDbo>() : rows.Select(x => x.PublicationFromYdb());
    }


    /// <summary>
    /// Получить пагинированный список публикаций
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public async Task<IEnumerable<PublicationDbo>> FindPublicationsInHeader(string key)
    {
        var rows = await _client.ExecuteFind($"""
                                              
                                                          DECLARE $prefix AS string;
                                              
                                                          {_select}
                                                          FROM {_scheme.Publication}
                                                          WHERE publication_header LIKE $prefix
                                                      
                                              """, new Dictionary<string, YdbValue>
        {
            {"$prefix", YdbValue.MakeString(Encoding.Default.GetBytes("%" + key + "%"))},
        });


        return rows is null ? Array.Empty<PublicationDbo>() : rows.Select(x => x.PublicationFromYdb());
    }


    private string _select =>
        "SELECT publication_id, publication_header, publication_text, publication_preview, publication_images_id, author_id, publication_time";

    public PublicationStorage(IYdbScheme scheme, YdbSerializerHelper<PublicationDbo> serializerHelper,
                              IYdbClient client)
        : base(scheme, serializerHelper, client)
    {
    }
}
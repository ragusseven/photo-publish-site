﻿using System.Text;
using PhotoProject.Infrastructure.Common;
using PhotoProject.Infrastructure.DAL.Models;
using Ydb.Sdk.Value;

namespace PhotoProject.Infrastructure.DAL.Repositories;

/// <summary>
/// Фотосессии
/// </summary>
public class PhotoSessionStorage
{
    private readonly IYdbScheme _scheme;
    private readonly YdbSerializerHelper<PhotoSessionDbo> _serializerHelper;
    private readonly IYdbClient _client;

    /// <summary>
    /// Фотосессии
    /// </summary>
    /// <param name="client"></param>
    /// <param name="scheme"></param>
    public PhotoSessionStorage(IYdbClient client, IYdbScheme scheme,
                               YdbSerializerHelper<PhotoSessionDbo> serializerHelper)
    {
        _scheme = scheme;
        this._serializerHelper = serializerHelper;
        _client = client;
    }

    public async Task<PhotoSessionDbo?> CreateAsync(PhotoSessionDbo dbo)
    {
        var ydb = _serializerHelper.GenerateInsert(dbo, _scheme.Photosession);
        var rows = await _client.ExecuteFind(ydb.Yql, ydb.Item2);

        if (rows is null) return null;
        return MapToModel(rows).Single();
    }

    public async Task<PhotoSessionDbo?> UpdateAsync(PhotoSessionDbo dbo)
    {
        var ydb = _serializerHelper.GenerateUpdate(dbo, _scheme.Photosession);
        await _client.ExecuteModify(ydb.Yql, ydb.Item2);

        return dbo;
    }

    public async Task DeleteAsync(Guid id)
    {
        var rows = await _client.ExecuteFind($@"
            DECLARE $id AS string;

            DELETE FROM {_scheme.Photosession}
            WHERE id = $id
        ", new Dictionary<string, YdbValue>
        {
            {"$id", YdbValue.MakeString(id.GetStringView())},
        });
    }


    /// <summary>
    /// Получить фотосессию по Id
    /// </summary>
    /// <param name="id">Id фотосессии</param>
    /// <returns></returns>
    ///
    /// public Guid Id { get; set; }
    public async Task<PhotoSessionDbo?> GetPhotoSession(Guid id)
    {
        var rows = await _client.ExecuteFind($@"
            DECLARE $id AS string;

            {_select}
            FROM {_scheme.Photosession}
            WHERE id = $id
        ", new Dictionary<string, YdbValue>
        {
            {"$id", YdbValue.MakeString(id.GetStringView())},
        });

        if (rows is null) return null;
        return MapToModel(rows).Single();
    }

    /// <summary>
    /// Получить все фотосессии по Id юзера
    /// </summary>
    /// <param name="userId">Id юзера</param>
    /// <returns></returns>
    public async Task<IEnumerable<PhotoSessionDbo>> GetUserPhotoSessions(Guid userId)
    {
        var rows = await _client.ExecuteFind($@"
            DECLARE $id AS string;

            {_select}
            FROM {_scheme.Photosession}
            WHERE user_id = $id
        ", new Dictionary<string, YdbValue>
        {
            {"$id", YdbValue.MakeString(userId.GetStringView())},
        });

        return rows is null ? Array.Empty<PhotoSessionDbo>() : MapToModel(rows);
    }

    public async Task<IEnumerable<PhotoSessionDbo>> GetUserOpenPhotoSessions(Guid userId)
    {
        return await GetUserPhotoSessions(userId, Status.Open);
    }

    public async Task<IEnumerable<PhotoSessionDbo>> GetUserClosedhotoSessions(Guid userId)
    {
        return await GetUserPhotoSessions(userId, Status.Closed);
    }

    private async Task<IEnumerable<PhotoSessionDbo>> GetUserPhotoSessions(Guid userId, Status status)
    {
        var rows = await _client.ExecuteFind($"""
                                              
                                                          DECLARE $id AS string;
                                                          DECLARE $status AS int32;
                                              
                                                          {_select}
                                                          FROM {_scheme.Photosession}
                                                          WHERE user_id = $id
                                                          AND status = $status
                                                      
                                              """, new Dictionary<string, YdbValue>
        {
            {"$id", YdbValue.MakeString(userId.GetStringView())},
            {"$status", YdbValue.MakeInt32((int) status)},
        });

        return rows is null ? Array.Empty<PhotoSessionDbo>() : MapToModel(rows);
    }

    private IEnumerable<PhotoSessionDbo> MapToModel(IEnumerable<ResultSet.Row> rows)
    {
        return rows.Select(row => new PhotoSessionDbo
        {
            Id = row["id"].GetOptionalString()!.GetGuid(),
            Name = Encoding.Default.GetString(row["name"].GetOptionalString() ?? Array.Empty<byte>()),
            Price = row["price"].GetOptionalInt32()!.Value,
            StartDate = row["start_date"].GetOptionalDatetime()!.Value,
            EndDate = row["end_date"].GetOptionalDatetime()!.Value,
            Status = (Status) row["status"].GetOptionalInt32()!,
            UserId = row["user_id"].GetOptionalString()!.GetGuid()
        });
    }

    private string _select => "SELECT id, name, price, start_date, end_date, status, user_id";
}
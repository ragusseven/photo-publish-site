using Ydb.Sdk.Value;

namespace PhotoProject.Infrastructure.DAL;

public interface IYdbClient
{
    Task ExecuteScheme(string query);
    
    Task<IEnumerable<ResultSet.Row>?> ExecuteFind(
        string query, Dictionary<string, YdbValue> parameters);

    Task ExecuteModify(string query, Dictionary<string, YdbValue> parameters);
}
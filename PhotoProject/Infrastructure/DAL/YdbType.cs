namespace PhotoProject.Infrastructure.DAL;

public enum YdbType
{
    Unknown,
    String,
    OptionalString,
    Guid,
    OptionalGuid,
    Int,
    OptionalInt,
    DateTime,
    OptionalDateTime,
    Bytes
}
namespace PhotoProject.Infrastructure.DAL;

public interface IYdbScheme
{
    public string Photosession { get; }
    public string Image { get; }
    public string Publication { get; }
    public string Record { get; }
    public string User { get; }
}
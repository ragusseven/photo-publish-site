namespace PaymentsBot;

public class Configuration
{
    public string YdbEndpoint => Environment.GetEnvironmentVariable(nameof(YdbEndpoint))!;
    public string YdbPath => Environment.GetEnvironmentVariable(nameof(YdbPath))!;
    public string? IamTokenPath => Environment.GetEnvironmentVariable(nameof(IamTokenPath))!;
}
using PhotoProject.Infrastructure.DAL;

namespace WebApi.Infrastructure.Attributes;

public class ColumnAttribute(string name, YdbType type) : Attribute
{
    public string Name = name;
    public YdbType Type = type;
}

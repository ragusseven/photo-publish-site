using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using PaymentsBot;
using PhotoProject.Infrastructure.Auth;
using PhotoProject.Infrastructure.DAL;
using PhotoProject.Infrastructure.DAL.Models;
using PhotoProject.Infrastructure.DAL.Repositories;

namespace PhotoProject
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }


        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddDistributedMemoryCache();
            services.AddSession();
            services.AddControllers();
            services.AddEndpointsApiExplorer();

            services.AddScoped<UserStorage>();
            services.AddSingleton<Func<UserStorage>>(sp =>
                () => sp.CreateScope().ServiceProvider.GetService<UserStorage>()
                      ?? throw new Exception("Realization not found"));

            services.AddScoped<UserStorage>();
            services.AddScoped<PublicationStorage>();
            services.AddScoped<ImageStorage>();
            services.AddScoped<PhotoSessionStorage>();
            services.AddScoped<RecordStorage>();

            services.AddSingleton<IYdbScheme, YdbScheme>();
            services.AddSingleton<IYdbClient, YdbClient>();
            services.AddSingleton<Configuration>();
            services.AddSingleton<YdbSerializerHelper<PublicationDbo>>();
            services.AddSingleton<YdbSerializerHelper<ImageDbo>>();
            services.AddSingleton<YdbSerializerHelper<PhotoSessionDbo>>();
            services.AddSingleton<YdbSerializerHelper<RecordDbo>>();
            services.AddSingleton<YdbSerializerHelper<UserDbo>>();

            services.AddScoped<RecordStorage>();
            services.AddScoped<RecordStorage>();


            services.AddSingleton<IJwtAuthManager, JwtAuthManager>();
            services.AddHostedService<JwtRefreshTokenCache>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "My API",
                    Version = "v1"
                });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = @"JWT Authorization header using the Bearer scheme.
                          Enter 'Bearer' [space] and then your token in the text input below.
                          Example: 'Bearer 12345abcdef'",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] { }
                    }
                });
            });
            services.AddAuthentication(x =>
                    {
                        x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                        x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    })
                    .AddJwtBearer(options =>
                    {
                        options.RequireHttpsMetadata = true;
                        options.SaveToken = true;
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidateAudience = true,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = JwtTokenConfig.Issuer,
                            ValidAudience = JwtTokenConfig.Audience,
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(JwtTokenConfig.Secret)),
                            ClockSkew = TimeSpan.FromMinutes(1)
                        };
                    });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
            app.UseCors(corsPolicyBuilder => corsPolicyBuilder.AllowAnyOrigin()
                                                              .AllowAnyHeader()
                                                              .AllowAnyMethod());
            //  if (app.Environment.IsDevelopment())
            // {
            app.UseSwagger();
            app.UseSwaggerUI();
            //app.UseHsts();
            // }

            app.UseHttpsRedirection();
            app.UseSession();
            app.Use(async (context, next) =>
            {
                var token = context.Session.GetString("Token");
                if (!string.IsNullOrEmpty(token))
                    context.Request.Headers.Add("Authorization", "Bearer " + token);
                await next();
            });
            app.UseAuthentication();
        }
    }
}
﻿using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;

namespace PhotoProject.Controllers;

/// <summary>
/// Базовый контроллер
/// </summary>
public abstract class BaseController : Controller
{
    /// <summary>
    /// Получить Id пользователя
    /// </summary>
    /// <returns>Возвращает Id пользователя</returns>
    protected Guid? GetUserIdOrNull()
    {
        var userIdString = User.FindFirstValue(ClaimTypes.NameIdentifier);
        if (string.IsNullOrWhiteSpace(userIdString))
            return null;

        return Guid.Parse(userIdString);
    }
}
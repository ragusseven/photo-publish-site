﻿using Microsoft.AspNetCore.Mvc;
using PhotoProject.Infrastructure.DAL.Models;
using PhotoProject.Infrastructure.DAL.Repositories;
using PhotoProject.Model;
using PhotoProject.Model.OutModel;

namespace PhotoProject.Controllers;

/// <summary>
/// Пользователи
/// </summary>
[ApiController]
[Route("users")]
public class UserController : BaseController
{
    private readonly PublicationStorage _publicationStorage;
    private readonly UserStorage _userStorage;
    private readonly PhotoSessionStorage _photoSessionStorage;

    /// <summary>
    /// Пользователи
    /// </summary>
    /// <param name="publicationStorage"></param>
    /// <param name="userStorage"></param>
    public UserController(PublicationStorage publicationStorage, PhotoSessionStorage photoSessionStorage, UserStorage userStorage)
    {
        _userStorage = userStorage;
        _publicationStorage = publicationStorage;
        _photoSessionStorage = photoSessionStorage;
        Console.WriteLine("Создали UserController");
    }

    /// <summary>
    /// Получить информации о пользователе по логину
    /// </summary>
    /// <param name="login">Логин пользователя</param>
    /// <returns>Возвращает всю информацию о пользователе</returns>
    [HttpGet("{login}")]
    public async Task<IActionResult> GetUserByLogin(string login)
    {
        var user = await _userStorage.GetByLogin(login);
        if (user is null)
            return StatusCode(StatusCodes.Status404NotFound, "User not found");

        return Ok(new User(user!.Login, user.Email, new UserExtraFields(user.Name, user.Surname, user.Photo), user.ProfileName));
    }

    /// <summary>
    /// Получить все публикации пользователя
    /// </summary>
    /// <param name="login">Логин пользователя</param>
    /// <returns>Возвращает все публикации пользователя</returns>
    [HttpGet("{login}/publications")]
    public async Task<IActionResult> GetUserPublications(string login)
    {
        var user = await _userStorage.GetByLogin(login);
        if (user is null)
            return StatusCode(StatusCodes.Status404NotFound, "User not found");

        var publications = await _publicationStorage.GetUserPublications(user!.Id);
        
        return Ok(publications.Select(x => new PublicationPreviewModel(
            user.Login, user.Name, user.Surname,
            x.Id, x.Header, x.Text, x.Preview, x.ImagesId.Split(';').ToArray(), user.ProfileName, x.DateTime)));
    }
    
    /// <summary>
    /// Получить все фотосессии пользователя
    /// </summary>
    /// <param name="login">Логин пользователя</param>
    /// <returns>Возвращает все фотосессии пользователя</returns>
    [HttpGet("{login}/photosessions")]
    public async Task<IActionResult> GetUserPhotoSessions(string login)
    {
        var user = await _userStorage.GetByLogin(login);
        if (user is null)
            return StatusCode(StatusCodes.Status404NotFound, "User not found");

        var photoSessions = await _photoSessionStorage.GetUserPhotoSessions(user!.Id);
        
        return Ok(ToPreviewModel(photoSessions.Select(x=>(x, user))));
    }
    
    /// <summary>
    /// Получить открытые для записи фотосессии пользователя
    /// </summary>
    /// <param name="login">Логин пользователя</param>
    /// <returns>Возвращает открытые для записи фотосессии пользователя</returns>
    [HttpGet("{login}/photosessions/open")]
    public async Task<IActionResult> GetUserOpenPhotoSessions(string login)
    {
        var user = await _userStorage.GetByLogin(login);
        if (user is null)
            return StatusCode(StatusCodes.Status404NotFound, "User not found");

        var photoSessions = await _photoSessionStorage.GetUserOpenPhotoSessions(user!.Id);
        
        return Ok(ToPreviewModel(photoSessions.Select(x=>(x, user))));
    }
    
    /// <summary>
    /// Получить закрытые для записи фотосессии пользователя
    /// </summary>
    /// <param name="login">Логин пользователя</param>
    /// <returns>Возвращает закрытые для записи фотосессии пользователя</returns>
    [HttpGet("{login}/photosessions/closed")]
    public async Task<IActionResult> GetUserClosedPhotoSessions(string login)
    {
        var user = await _userStorage.GetByLogin(login);
        if (user is null)
            return StatusCode(StatusCodes.Status404NotFound, "User not found");

        var photoSessions = await _photoSessionStorage.GetUserClosedhotoSessions(user!.Id);
        
        return Ok(ToPreviewModel(photoSessions.Select(x=>(x, user))));
    }
    
    private static IEnumerable<PhotoSessionPreviewModel> ToPreviewModel(
        IEnumerable<(PhotoSessionDbo, UserDbo)> photoSessions) =>
        photoSessions
            .Select(x => new PhotoSessionPreviewModel(
                x.Item2.Login, x.Item2.Name, x.Item2.Surname, x.Item2.ProfileName,
                x.Item1.Id, x.Item1.Name, x.Item1.Price, x.Item1.StartDate, x.Item1.EndDate, x.Item1.Status));
}
﻿using Microsoft.AspNetCore.Mvc;
using PhotoProject.Infrastructure.DAL.Models;
using PhotoProject.Infrastructure.DAL.Repositories;
using PhotoProject.Model;

namespace PhotoProject.Controllers;

/// <summary>
/// Фотосессии и записи
/// </summary>
[ApiController]
[Route("[controller]")]
public class PhotoSessionController : BaseController
{
    private readonly PhotoSessionStorage _photoSessionStorage;
    private readonly ImageStorage _imageStorage;
    private readonly RecordStorage _recordStorage;
    private readonly UserStorage _userStorage;

    /// <summary>
    /// Фотосессии и записи
    /// </summary>
    /// <param name="photoSessionStorage"></param>
    /// <param name="userStorage"></param>
    /// <param name="imageStorage"></param>
    /// <param name="recordStorage"></param>
    public PhotoSessionController(PhotoSessionStorage photoSessionStorage, UserStorage userStorage,
                                  ImageStorage imageStorage, RecordStorage recordStorage)
    {
        _photoSessionStorage = photoSessionStorage;
        _userStorage = userStorage;
        _imageStorage = imageStorage;
        _recordStorage = recordStorage;
        Console.WriteLine("Создали PhotoSessionController");
    }

    /// <summary>
    /// Получить фотосессию по Id
    /// </summary>
    /// <param name="id">Id фотосессии</param>
    /// <returns></returns>
    [HttpGet("~/photosessions/{id}")]
    public async Task<IActionResult> GetPhotoSession(Guid id)
    {
        var photoSession = await _photoSessionStorage.GetPhotoSession(id) ?? null;
        if (photoSession is null)
            return StatusCode(StatusCodes.Status404NotFound, "PhotoSession not found");

        var user = await _userStorage.GetById(photoSession.UserId);

        return Ok(new PhotoSessionPreviewModel(
            user.Login, user.Name, user.Surname,
            user.ProfileName,
            photoSession.Id, photoSession.Name, photoSession.Price, photoSession.StartDate, photoSession.EndDate,
            photoSession.Status));
    }

    /// <summary>
    /// Удалить фотосессию по Id
    /// </summary>
    /// <param name="id">Id фотосессии</param>
    /// <returns></returns>
    [HttpDelete("~/photosessions/{id}")]
    public async Task<IActionResult> DeletePhotoSession(Guid id)
    {
        var ownerId = GetUserIdOrNull();
        if (ownerId is null)
            return StatusCode(StatusCodes.Status403Forbidden, "You are not logged in");
        var photoSession = _photoSessionStorage.GetPhotoSession(id).Result;
        if (photoSession is null)
            return StatusCode(StatusCodes.Status404NotFound, "PhotoSession not found");
        var authorId = photoSession.UserId;
        if (authorId != ownerId)
            return StatusCode(StatusCodes.Status403Forbidden, "You are not the author of the photoSession");
        await _photoSessionStorage.DeleteAsync(id);
        return StatusCode(StatusCodes.Status204NoContent, "PhotoSession was successfully deleted");
    }

    /// <summary>
    /// Изменить фотосессию по Id 
    /// </summary>
    /// <param name="id">Id фотосессии</param>
    /// <param name="name">Название фотосессии</param>
    /// <param name="startDate">Начало фотосессии</param>
    /// <param name="endDate">Конец фотосессии</param>
    /// <param name="address">Адрес фотосессии</param>
    /// <param name="price">Цена фотосессии</param>
    /// <param name="status">Статус фотосессии</param>
    /// <param name="previewFile">Превью фотосессии</param>
    /// <returns></returns>
    [HttpPut("~/photosessions/{id}")]
    public async Task<IActionResult> UpdatePhotoSession(Guid id, [FromForm] string name, [FromForm] DateTime startDate,
                                                        [FromForm] DateTime endDate, [FromForm] int price,
                                                        [FromForm] Status status)
    {
        var ownerId = GetUserIdOrNull();
        if (ownerId is null)
            return StatusCode(StatusCodes.Status403Forbidden, "You are not logged in");
        var photoSession = _photoSessionStorage.GetPhotoSession(id).Result;
        if (photoSession is null)
            return StatusCode(StatusCodes.Status404NotFound, "PhotoSession not found");
        var authorId = photoSession.UserId;
        if (authorId != ownerId)
            return StatusCode(StatusCodes.Status403Forbidden, "You are not the author of the photoSession");


        var updatedPhotoSession = new PhotoSessionDbo
        {
            Name = name,
            StartDate = startDate,
            EndDate = endDate,
            Price = price,
            Status = status,
            UserId = (Guid) ownerId,
        };

        await _photoSessionStorage.UpdateAsync(updatedPhotoSession);

        return Ok(updatedPhotoSession.Id);
    }

    /// <summary>
    /// Создать фотосессию
    /// </summary>
    /// <param name="name">Название фотосессии</param>
    /// <param name="startDate">Начало фотосессии</param>
    /// <param name="endDate">Конец фотосессии</param>
    /// <param name="address">Адрес фотосессии</param>
    /// <param name="price">Цена фотосессии</param>
    /// <param name="status">Статус фотосессии</param>
    /// <param name="previewFile">Превью фотосессии</param>
    /// <returns></returns>
    [HttpPost("~/photosessions")]
    public async Task<IActionResult> CreatePhotoSession([FromForm] string name, [FromForm] DateTime startDate,
                                                        [FromForm] DateTime endDate, [FromForm] int price,
                                                        [FromForm] Status status)
    {
        var ownerId = GetUserIdOrNull();
        if (ownerId is null)
            return StatusCode(StatusCodes.Status403Forbidden, "You are not logged in");

        var photoSession = new PhotoSessionDbo
        {
            Id = Guid.NewGuid(),
            Name = name,
            StartDate = startDate,
            EndDate = endDate,
            Price = price,
            Status = status,
            UserId = (Guid) ownerId,
        };

        await _photoSessionStorage.CreateAsync(photoSession);

        return Ok(photoSession.Id);
    }

    /// <summary>
    /// Создать запись на фотосессию
    /// </summary>
    /// <param name="id">Id фотосессии</param>
    /// <param name="email">Почта записавшегося</param>
    /// <returns></returns>
    [HttpPost("~/photosessions/{id}/records")]
    public async Task<IActionResult> CreateRecord(Guid id, [FromForm] string email, [FromForm] string name)
    {
        var photoSession = await _photoSessionStorage.GetPhotoSession(id) ?? null;
        if (photoSession is null)
            return StatusCode(StatusCodes.Status404NotFound, "PhotoSession not found");
        if (photoSession.Status == Status.Closed)
            return StatusCode(StatusCodes.Status418ImATeapot, "Record is closed");
        var record = new RecordDbo
        {
            Id = Guid.NewGuid(),
            Status = Status.Open,
            Email = email,
            Name = name,
            PhotoSessionDboId = photoSession.Id
        };

        await _recordStorage.CreateAsync(record);

        return Ok(record.Id);
    }

    /// <summary>
    /// Получить запись по Id и по Id фотосессии
    /// </summary>
    /// <param name="id">Id фотосессии</param>
    /// <param name="recordId">Id записи</param>
    /// <returns></returns>
    [HttpGet("~/photosessions/{id}/records/{recordId}")]
    public async Task<IActionResult> GetRecord(Guid id, Guid recordId)
    {
        var photoSession = await _photoSessionStorage.GetPhotoSession(id) ?? null;
        if (photoSession is null)
            return StatusCode(StatusCodes.Status404NotFound, "PhotoSession not found");
        var record = await _recordStorage.GetRecord(recordId) ?? null;
        if (record is null)
            return StatusCode(StatusCodes.Status404NotFound, "Record not found");

        return Ok(new RecordPreviewModel(record.Id, record.PhotoSessionDboId, record.Email, record.Status,
            record.Name));
    }

    /// <summary>
    /// Изменить статус фотосессии по Id и Id фотосессии
    /// </summary>
    /// <param name="id">Id фотосессии</param>
    /// <param name="recordId">Id записи</param>
    /// <param name="status">Статус записи</param>
    /// <returns></returns>
    [HttpPut("~/photosessions/{id}/records/{recordId}")]
    public async Task<IActionResult> UpdateRecord(Guid id, Guid recordId, [FromForm] Status status)
    {
        var ownerId = GetUserIdOrNull();
        if (ownerId is null)
            return StatusCode(StatusCodes.Status403Forbidden, "You are not logged in");
        var photoSession = _photoSessionStorage.GetPhotoSession(id).Result;
        if (photoSession is null)
            return StatusCode(StatusCodes.Status404NotFound, "PhotoSession not found");
        var authorId = photoSession.UserId;
        if (authorId != ownerId)
            return StatusCode(StatusCodes.Status403Forbidden, "You are not the author of the photoSession");
        var record = _recordStorage.GetRecord(recordId).Result;
        if (record is null)
            return StatusCode(StatusCodes.Status404NotFound, "Record not found");
        if (status == Status.Open)
            return StatusCode(StatusCodes.Status400BadRequest, "You can not change status to Open");

        var updatedRecord = new RecordDbo
        {
            Id = record.Id,
            PhotoSessionDboId = record.PhotoSessionDboId,
            Email = record.Email,
            Status = status,
            Name = record.Name
        };

        await _recordStorage.DeleteAllExceptOne(photoSession.Id, updatedRecord.Id);
        await _recordStorage.DeleteAsync(record.Id);
        await _recordStorage.CreateAsync(updatedRecord);

        var updatedPhotoSession = new PhotoSessionDbo
        {
            Id = photoSession.Id,
            Name = photoSession.Name,
            StartDate = photoSession.StartDate,
            EndDate = photoSession.EndDate,
            Price = photoSession.Price,
            Status = status,
            UserId = (Guid) ownerId,
        };
        await _photoSessionStorage.UpdateAsync(updatedPhotoSession);
        return Ok(updatedRecord.Id);
    }

    /// <summary>
    /// Получить все записи на фотосессию по Id
    /// </summary>
    /// <param name="id">Id фотосессии</param>
    /// <returns></returns>
    [HttpGet("~/photosessions/{id}/records")]
    public async Task<IActionResult> GetRecords(Guid id)
    {
        var records = await _recordStorage.GetRecordByPhotoSession(id);

        return Ok(ToRecordPreviewModel(records));
    }

    private async Task<Guid> CreateImage(IFormFile file)
    {
        await using var fileStream = file.OpenReadStream();
        var bytesImage = new byte[file.Length];
        await fileStream.ReadAsync(bytesImage, 0, (int) file.Length);
        fileStream.Close();
        var image = new ImageDbo {Id = Guid.NewGuid(), Image = bytesImage};
        return image.Id;
    }

    private static IEnumerable<PhotoSessionPreviewModel> ToPhotoSessionPreviewModel(
        IEnumerable<(PhotoSessionDbo photoSession, UserDbo user)> photoSessions) =>
        photoSessions
            .Select(x => new PhotoSessionPreviewModel(
                x.user.Login, x.user.Name, x.user.Surname, x.user.ProfileName,
                x.photoSession.Id, x.photoSession.Name, x.photoSession.Price, x.photoSession.StartDate,
                x.photoSession.EndDate, x.photoSession.Status));

    private static IEnumerable<RecordPreviewModel> ToRecordPreviewModel(
        IEnumerable<RecordDbo> photoSessions) =>
        photoSessions
            .Select(x => new RecordPreviewModel(x.Id, x.PhotoSessionDboId, x.Email, x.Status, x.Name));
}
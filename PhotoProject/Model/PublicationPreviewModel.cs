namespace PhotoProject.Model;

public record PublicationPreviewModel(string AuthorLogin, string? AuthorName, string? AuthorSurname, Guid Id,
                                   string Header, string? Text, string Preview, string[] Images,
                                   string AuthorProfileName, DateTime DateTime);
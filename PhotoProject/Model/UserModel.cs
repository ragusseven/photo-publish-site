﻿using PhotoProject.Infrastructure.DAL.Models;

namespace PhotoProject.Model;

/// <summary>
/// 
/// </summary>
public record UserModel(Guid Id, string Login, string Name, string Surname, string ProfileName);
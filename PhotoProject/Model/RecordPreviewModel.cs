﻿using PhotoProject.Infrastructure.DAL.Models;

namespace PhotoProject.Model;

public class RecordPreviewModel
{
    public RecordPreviewModel(Guid id, Guid photoSessionId, string email, Status status, string name)
    {
        Id = id;
        PhotoSessionId = photoSessionId;
        Email = email;
        Status = status;
        Name = name;
    }

    public Guid Id { get; set; }
    public string Email { get; set; }
    public Guid PhotoSessionId { get; set; }
    public Status Status { get; set; }

    public string Name { get; set; }
}
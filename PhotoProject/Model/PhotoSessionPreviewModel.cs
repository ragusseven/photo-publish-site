﻿using PhotoProject.Infrastructure.DAL.Models;

namespace PhotoProject.Model;

public record PhotoSessionPreviewModel(string AuthorLogin, string? AuthorName, string? AuthorSurname,
                                    string AuthorProfileName, Guid Id,
                                    string Name, int Price, DateTime StartDate, DateTime EndDate, Status Status);